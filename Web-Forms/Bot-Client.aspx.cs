﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Bot_Client : System.Web.UI.Page
{
    protected string directLineSecret = "FcjTpWWaTdg.byWgGx0o9sZdPwwLcUU1sslZu99jBIeDQOTtgqktWP0";    // Could also read from the configuration (Web.Config > appSettings)
    public string DirectLineEphemeralToken = "<More secure Direct Line ephemeral token>";
    public string TokenResponseFromService = "<Token Response From Service>";
    public string DummyTokenObject = "<Token Response From Service>";
    public string UserID = "dl_GUID";
    public string UserName = "WebChat User";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(directLineSecret)) throw new MissingFieldException("App configuration is missing the 'DirectLineSecret' field.");
        #region exchanging Direct Line secret for a more secure ephemeral token
        // Reference: https://docs.microsoft.com/en-us/azure/bot-service/rest-api/bot-framework-rest-direct-line-3-0-authentication#generate-token

        // If we expect thousands users per minute requesting this bot page, the below code could lead to "Dynamic TCP port exhaustion for outbound network calls".
        // One article explaining this: https://blog.coeo.com/emmazambonini/2016/06/01/ephemeral-port-exhaustion
        // To avoid such port exhaustion, a connection factory pattern should be used. A good article seems to be:
        // https://www.baeldung.com/httpclient-connection-management
        // Another potential pitfall: ReadAsStringAsync().Result is actually doing a thread block until the response from token service arrives...
        // With thousands users per minute requesting this bot page, this could lead to thread starvation.

        // The HttpClient needs the System.Net.Http namespace, so the assembly needs to be added in the Web.config:
        // <compilation> <assemblies> <add assembly = "System.Net.Http, Version=4.2.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A" /> </assemblies> </compilation>
        using (HttpClient httpClient = new HttpClient())
        {
            using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage())
            {
                httpRequestMessage.Method = HttpMethod.Post;
                httpRequestMessage.RequestUri = new Uri("https://directline.botframework.com/v3/directline/tokens/generate");
                httpRequestMessage.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", directLineSecret);
                UserID = string.Format("dl_{0}", Guid.NewGuid());
                //string httpRequestBodyPayload = $"{{ \"user\": {{ \"id\": \"{UserID}\", \"name\": \"{UserName}\" }} }}";
                string httpRequestBodyPayload = string.Format("{{ \"user\": {{ \"id\": \"{0}\", \"name\": \"{1}\" }} }}", UserID, UserName);
                httpRequestMessage.Content = new StringContent(httpRequestBodyPayload, Encoding.UTF8, "application/json");
                HttpResponseMessage httpResponseMessage = httpClient.SendAsync(httpRequestMessage).Result;
                string token = String.Empty;
                if (httpResponseMessage.IsSuccessStatusCode)
                {
                    TokenResponseFromService = httpResponseMessage.Content.ReadAsStringAsync().Result;
                    JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                    TokenReponse parsedJsonResponse = javaScriptSerializer.Deserialize<TokenReponse>(TokenResponseFromService);
                    DirectLineEphemeralToken = parsedJsonResponse.token;
                }
                else
                {
                    throw new HttpRequestException("Could not contact secret token service at https://directline.botframework.com/v3/directline/tokens/generate");
                    // Here we should probably let the user know that we can't start a conversation with the bot
                }
            }
        }
        #endregion
    }
}

public class TokenReponse
{
    public string conversationId { get; set; }
    public string token { get; set; }
    public int expires_in { get; set; }
}
