﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="bot-client.aspx.cs" Inherits="Bot_Client" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>IE WebChat client, securely</title>
    <script crossorigin="anonymous" src="https://cdn.botframework.com/botframework-webchat/latest/webchat-es5.js"></script>
    <style type="text/css">
        html,
        body {
            height: 100%;
            background-color: aqua;
            font-family: Verdana, Arial, Arial, Helvetica, sans-serif;
            color: #FEFEFE;
            text-align: center;
            margin: 0;
        }
        h1, h2, h3 { font-weight: normal; }
        h1 { font-size: 2.0em; }
        h2 { font-size: 1.6em; }
        #webChat {
            margin: 1.0em auto auto auto;
            height: 70%;
            width: 50%;
        }
    </style>
</head>
<body>
    <article>
        <h1>Tests for bot</h1>
        <h2>Try to engage the bot from a web-forms page, using the WebChat control</h2>
        <!--<p>Token Response From Service:<br /><%= TokenResponseFromService %></p>-->
        <p>Direct Line Ephemeral Token:<br /><%= DirectLineEphemeralToken %></p>
    </article>
    <div id="webChat" role="main"></div>
    <script type="text/javascript">
        window.WebChat.renderWebChat(
            {
                directLine: window.WebChat.createDirectLine({
                    token: '<%= DirectLineEphemeralToken %>'
                    }),
                userID: '<%= UserID %>',
                username: '<%= UserName %>',
                locale: 'en-US',
                botAvatarInitials: 'WC',
                userAvatarInitials: 'WW'
            },
            document.getElementById('webChat')
        );
        document.querySelector('#webChat > *').focus();
    </script>
</body>
</html>
