﻿var MyPageDOM = {
    botHandleName: null,
    botDirectLineSecret: null,
    Events: {
        btnValidation_Click: function () {
            jQuery("#ValidationError").slideUp(500, function () { jQuery("#BotBasics").slideDown(); });
            return;
        },
        btnSubmit_Click: function () {
            MyPageDOM.botHandleName = jQuery("input[name='BotAttributes.BotHandle']").val();
            MyPageDOM.botDirectLineSecret = jQuery("input[name='BotAttributes.WebChatSecret']").val();
            if (!MyPageDOM.botHandleName || !MyPageDOM.botDirectLineSecret) {
                jQuery("#BotBasics").slideUp(500, function () { jQuery("#ValidationError").slideDown(); });
                return;
            }
            jQuery("#BotBasics form").submit();
        }
    }
};



function Page_Start() {
    if (jQuery("#BotBasics")) {
        jQuery("#BotBasics input[name='BotHandle']").focus();
    }
}



jQuery(Page_Start);