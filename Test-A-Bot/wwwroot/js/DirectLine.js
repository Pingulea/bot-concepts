﻿var MyPageDOM = {
    botHandleName: null,
    botDirectLineSecret: null,
    Events: {
        btnValidation_Click: function () {
            jQuery("#ValidationError").slideUp(500, function () { jQuery("#BotBasics").slideDown(); });
            return;
        },
        btnSubmit_Click: function () {
            MyPageDOM.botHandleName = jQuery("input[name='BotHandle']").val();
            MyPageDOM.botDirectLineSecret = jQuery("input[name='DirectLineSecret']").val();
            if (!MyPageDOM.botHandleName || !MyPageDOM.botDirectLineSecret) {
                jQuery("#BotBasics").slideUp(500, function () { jQuery("#ValidationError").slideDown(); });
                return;
            }
            jQuery("#txtBotHandle").text(MyPageDOM.botHandleName);
            jQuery("#BotBasics").slideUp(500, function () { jQuery("#BotClient").slideDown(); jQuery("#BotDetails").slideDown(); });
            MyPageDOM.StartTheBotClient();
        }
    },
    StartTheBotClient: function () {
        console.debug("Connecting to Direct Line channel. Secret: " + MyPageDOM.botDirectLineSecret);
        // apply some customizations as per options available at
        // https://github.com/microsoft/BotFramework-WebChat/blob/master/packages/component/src/Styles/defaultStyleOptions.js
        const styleOptions = {
            backgroundColor: "#F0F0F0",
            bubbleBackground: "rgba(0, 128, 255, .3)",
            bubbleFromUserBackground: "rgba(0, 255, 128, .3)",
            sendBoxBackground: "rgba(128, 0, 255, .3)",
            sendBoxHeight: 48,
            userID: "Test-a-Bot User",
            username: "Web Chat User",
            locale: "en-US",
            botAvatarInitials: "BT",
            userAvatarInitials: "TB"
        };
        window.WebChat.renderWebChat(
            {
                directLine: window.WebChat.createDirectLine({
                    secret: MyPageDOM.botDirectLineSecret
                }),
                styleOptions
            },
            document.getElementById("webChat")
        );
        document.querySelector("#webChat > *").focus();
    }
};



function Page_Start() {
    jQuery("#BotBasics input[name='BotHandle']").focus();
}



jQuery(Page_Start);