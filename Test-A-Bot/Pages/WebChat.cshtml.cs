﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Test_A_Bot.Pages
{
    public class WebChatModel : PageModel
    {
        [BindProperty]
        public WebChatBotFields BotAttributes { get; set; }

        private readonly ILogger<WebChatModel> _logger;
        private readonly IConfiguration _configuration;

        public WebChatModel(ILogger<WebChatModel> logger, IConfiguration configuration)
        {
            this._logger = logger;
            this._configuration = configuration;
        }

        public IActionResult OnGet()
        {
            this.BotAttributes = new WebChatBotFields()
            {
                BotHandle = this._configuration["DefaultBot:HandleName"],
                WebChatSecret = this._configuration["DefaultBot:WebChatSecretKey"]
            };
            if (!string.IsNullOrWhiteSpace(BotAttributes.BotHandle)) _logger.LogInformation($"Default bot handle name configured: {BotAttributes.BotHandle}");
            else _logger.LogInformation("No default bot handle name configured.");
            if (!string.IsNullOrWhiteSpace(BotAttributes.WebChatSecret)) _logger.LogInformation($"Default bot WebChat secret configured: {BotAttributes.WebChatSecret}");
            else _logger.LogInformation("No default bot WebChat secret configured.");
            return Page();
        }

        public IActionResult OnPost()
        {
            return Page();
        }
    }
    public class WebChatBotFields
    {
        public string BotHandle { get; set; } = null;
        public string WebChatSecret { get; set; } = null;
        public bool HasInput
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.BotHandle)) return false;
                if (string.IsNullOrWhiteSpace(this.WebChatSecret)) return false;
                return true;
            }
        }
    }
}