﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Test_A_Bot.Pages
{
    public class WebChatWelcomeModel : PageModel
    {
        [BindProperty]
        public WebChatWelcomeBotFields BotAttributes { get; set; }

        private readonly ILogger<WebChatWelcomeModel> _logger;
        private readonly IConfiguration _configuration;

        public WebChatWelcomeModel(ILogger<WebChatWelcomeModel> logger, IConfiguration configuration)
        {
            this._logger = logger;
            this._configuration = configuration;
        }

        public IActionResult OnGet()
        {
            this.BotAttributes = new WebChatWelcomeBotFields()
            {
                BotHandle = this._configuration["DefaultBot:HandleName"],
                WebChatSecret = this._configuration["DefaultBot:WebChatSecretKey"]
            };
            if (!string.IsNullOrWhiteSpace(BotAttributes.BotHandle)) _logger.LogInformation($"Default bot handle name configured: {BotAttributes.BotHandle}");
            else _logger.LogInformation("No default bot handle name configured.");
            if (!string.IsNullOrWhiteSpace(BotAttributes.WebChatSecret)) _logger.LogInformation($"Default bot WebChat secret configured: {BotAttributes.WebChatSecret}");
            else _logger.LogInformation("No default bot WebChat secret configured.");
            return Page();
        }

        public async Task<IActionResult> OnPost()
        {
            this.BotAttributes.EphemeralToken = await this.GetEphemeralToken(this.BotAttributes.WebChatSecret);
            return Page();
        }

        private async Task<string> GetEphemeralToken(string WebChatOrDirectLineSecret)
        {
            if (string.IsNullOrWhiteSpace(WebChatOrDirectLineSecret)) throw new ArgumentException("The DirectLine or WebChat secret is not passed properly");
            string ephemeralToken = null;
            using (HttpClient httpClient = new HttpClient())
            {
                using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage())
                {
                    httpRequestMessage.Method = HttpMethod.Post;
                    httpRequestMessage.RequestUri = new Uri("https://directline.botframework.com/v3/directline/tokens/generate");
                    httpRequestMessage.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", WebChatOrDirectLineSecret);
                    string userId = $"dl_{Guid.NewGuid()}";
                    httpRequestMessage.Content = new StringContent(
                    JsonConvert.SerializeObject(
                        new { User = new { Id = userId } }),
                        Encoding.UTF8,
                        "application/json");
                    HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);
                    string token = String.Empty;
                    if (httpResponseMessage.IsSuccessStatusCode)
                    {
                        string httpResponseBody = await httpResponseMessage.Content.ReadAsStringAsync();
                        ephemeralToken = JsonConvert.DeserializeObject<DirectLineToken>(httpResponseBody).token;
                    }
                    else
                    {
                        throw new HttpRequestException("Could not contact secret token service at https://directline.botframework.com/v3/directline/tokens/generate");
                    }
                }
            }
            return ephemeralToken;
        }
    }

    public class WebChatWelcomeBotFields
    {
        public string BotHandle { get; set; } = null;
        public string WebChatSecret { get; set; } = null;
        public string EphemeralToken { get; set; } = null;
        public bool HasInput
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.BotHandle)) return false;
                if (string.IsNullOrWhiteSpace(this.WebChatSecret)) return false;
                if (string.IsNullOrWhiteSpace(this.EphemeralToken)) return false;
                return true;
            }
        }
    }

    public class DirectLineToken
    {
        public string conversationId { get; set; }
        public string token { get; set; }
        public int expires_in { get; set; }
    }
}