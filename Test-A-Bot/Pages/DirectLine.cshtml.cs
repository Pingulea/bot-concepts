﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Test_A_Bot.Pages
{
    public class DirectLineModel : PageModel
    {
        [BindProperty]
        public DirectLineBotFields BotAttributes { get; set; }

        private readonly ILogger<DirectLineModel> _logger;
        private readonly IConfiguration _configuration;

        public DirectLineModel(ILogger<DirectLineModel> logger, IConfiguration configuration)
        {
            this._logger = logger;
            this._configuration = configuration;
        }

        public void OnGet()
        {
            this.BotAttributes = new DirectLineBotFields()
            {
                BotHandle = this._configuration["DefaultBot:HandleName"],
                DirectLineSecret = this._configuration["DefaultBot:DirectLineSecretKey"]
            };
            if (!string.IsNullOrWhiteSpace(BotAttributes.BotHandle)) _logger.LogInformation($"Default bot handle name configured: {BotAttributes.BotHandle}");
            else _logger.LogInformation("No default bot handle name configured.");
            if (!string.IsNullOrWhiteSpace(BotAttributes.DirectLineSecret)) _logger.LogInformation($"Default bot DirectLine secret configured: {BotAttributes.DirectLineSecret}");
            else _logger.LogInformation("No default bot DirectLine secret configured.");
        }
    }

    public class DirectLineBotFields
    {
        public string BotHandle { get; set; } = null;
        public string DirectLineSecret { get; set; } = null;
        public bool HasInput
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.BotHandle)) return false;
                if (string.IsNullOrWhiteSpace(this.DirectLineSecret)) return false;
                return true;
            }
        }
    }
}
