﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;
using PinguBotWhoAmI.Dialogs;

namespace PinguBotWhoAmI.Bots
{
    public class AuthDialogBot : ActivityHandler
    {
        public AuthDialogBot(ConversationState conversationState, UserState userState, AuthDialog authDialog)
        {
            ConversationState = conversationState;
            UserState = userState;
            Dialog = authDialog;
        }

        protected readonly BotState ConversationState;
        protected readonly Dialog Dialog;
        protected readonly ILogger Logger;
        protected readonly BotState UserState;

        protected override async Task OnMembersAddedAsync(IList<ChannelAccount> membersAdded, ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {
            foreach (ChannelAccount member in membersAdded)
            {
                //OAuthPrompt oAuthPrompt = new OAuthPrompt(nameof(OAuthPrompt));
                if (member.Id != turnContext.Activity.Recipient.Id)
                {
                    if (string.IsNullOrWhiteSpace(member.Name))
                        await turnContext.SendActivityAsync(MessageFactory.Text("Hello and welcome!"), cancellationToken);
                    else
                        await turnContext.SendActivityAsync(MessageFactory.Text($"Hello and welcome, {member.Name}!"), cancellationToken);
                    
                    /*
                    TokenResponse tokenResponse = await oAuthPrompt.GetUserTokenAsync(turnContext, cancellationToken);
                    if (tokenResponse == null)
                    {
                        await turnContext.SendActivityAsync(MessageFactory.Text("I need to authenticate you, to see if you are authorized."), cancellationToken);

                        // oAuthPrompt.RunAsync(turnContext, this.ConversationState, cancellationToken)
                        // TO DO: Make sure to save the user's token in the user context/state, because here we're not in a Dialog context.
                    }
                    else
                    {
                        await turnContext.SendActivityAsync(MessageFactory.Text("I have authenticated you recently; you are 'logged in', so to say."), cancellationToken);
                        await turnContext.SendActivityAsync(MessageFactory.Text("Type 'token', if you want to see your user token OAuth JWT from Azure Active Directory."));
                        await turnContext.SendActivityAsync(MessageFactory.Text("Type 'logout', if you want to be logged out and cause a new authentication."));
                        await turnContext.SendActivityAsync(MessageFactory.Text("Type 'login' to trigger a new authentication roundtrip."));
                    }
                    */
                }
            }
        }
    }
}
