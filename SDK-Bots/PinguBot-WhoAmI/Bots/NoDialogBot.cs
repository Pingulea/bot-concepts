// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Bot.Schema.Teams;
using Microsoft.Extensions.Configuration;

namespace PinguBotWhoAmI.Bots
{
    /// <summary>
    ///     <para>This bot should peform the user authentication without employing a DialogSet/Dialogs.</para>
    ///     <para>Currently, it seems that OAuthPrompt cannot be used outside the context of a Dialog.</para>
    ///     <para>OAuthPrompt.GetUserTokenAsync() gets the user token, but does not initiate the login.</para>
    /// </summary>
    public class NoDialogBot : ActivityHandler
    {
        private string[] delayTriggerringWords = { "Delay", "Latency", "Slow", "Sleep" };
        private string[] errorTriggerringWords = { "Error", "Exception", "Throw", "Break", "Crash" };

        public NoDialogBot(IConfiguration configuration, ConversationState conversationState)
        {
            this.Configuration = configuration;
            this.ConversationState = conversationState;
            this.oAuthPromptSettings = new OAuthPromptSettings()
            {
                ConnectionName = Configuration["OAuth-UserAuthentication-ConnectionName"],
                Text = "Please sign in",
                Title = "Sign In",
                Timeout = 300000, // User has 5 minutes to login (1000 * 60 * 5)
            };
        }

        protected override async Task OnMessageActivityAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            if (errorTriggerringWords.Contains(turnContext.Activity.Text, comparer: System.StringComparer.InvariantCultureIgnoreCase))
            {
                await turnContext.SendActivityAsync(MessageFactory.Text("The Bot has received an exception-triggering word..."), cancellationToken);
                await turnContext.SendActivityAsync(MessageFactory.Text($"You just said: '{turnContext.Activity.Text}'"), cancellationToken);
                throw new System.ApplicationException($"The Bot has received an exception-triggering word: '{turnContext.Activity.Text}'");
            }
            if (delayTriggerringWords.Contains(turnContext.Activity.Text, comparer: System.StringComparer.InvariantCultureIgnoreCase))
            {
                await turnContext.SendActivityAsync(MessageFactory.Text($"The Bot has received a delay-triggering word..."), cancellationToken);
                Thread.Sleep(20000);
                await turnContext.SendActivityAsync(MessageFactory.Text($"You just said: '{turnContext.Activity.Text}'"), cancellationToken);
            }
            if (string.Compare(turnContext.Activity.Text, "token", System.StringComparison.OrdinalIgnoreCase) == 0)
            {
                await turnContext.SendActivityAsync(MessageFactory.Text("Your user token (OAuth JWT from Azure Active Directory) below:"), cancellationToken);
                // TO DO: display user token from the user context/state? We should avoid making one more call to token service.
                // TO DO: if the token is expired, we should renew? Or maybe the Token service does it for us...
                OAuthPrompt oAuthPrompt = new OAuthPrompt(nameof(OAuthPrompt), oAuthPromptSettings);
                TokenResponse tokenResponse = await oAuthPrompt.GetUserTokenAsync(turnContext, cancellationToken);
                oAuthPrompt.RunAsync()
                if (tokenResponse == null)
                {
                    await turnContext.SendActivityAsync(MessageFactory.Text("Your user token does not exist or it is expired. You should re-login."));
                    await turnContext.SendActivityAsync(MessageFactory.Text("Type 'login' to trigger a new authentication roundtrip."));
                }
                else
                {
                    await turnContext.SendActivityAsync(MessageFactory.Text(tokenResponse.Token.ToString()), cancellationToken);
                }
                return;
            }
            if (string.Compare(turnContext.Activity.Text, "logout", System.StringComparison.OrdinalIgnoreCase) == 0)
            {
                OAuthPrompt oAuthPrompt = new OAuthPrompt(nameof(OAuthPrompt), oAuthPromptSettings);
                await oAuthPrompt.SignOutUserAsync(turnContext, cancellationToken);
                await turnContext.SendActivityAsync(MessageFactory.Text("Your user was deleted and you are now 'logged out', so to say."));
                await turnContext.SendActivityAsync(MessageFactory.Text("Type 'login' to trigger a new authentication roundtrip."));
                return;
            }
            if (string.Compare(turnContext.Activity.Text, "login", System.StringComparison.OrdinalIgnoreCase) == 0)
            {
                OAuthPrompt oAuthPrompt = new OAuthPrompt(nameof(OAuthPrompt), oAuthPromptSettings);
                TokenResponse tokenResponse = await oAuthPrompt.GetUserTokenAsync(turnContext, cancellationToken);
                if (tokenResponse == null)
                {
                    await turnContext.SendActivityAsync(MessageFactory.Text("The authentication failed, perhaps you should retry by typing 'login' again."), cancellationToken);
                    // TO DO: Make sure to save the user's token in the user context/state, because here we're not in a Dialog context.
                }
                else
                {
                    await turnContext.SendActivityAsync(MessageFactory.Text("Authentication is successfull. You are 'logged in', so to say."), cancellationToken);
                    await turnContext.SendActivityAsync(MessageFactory.Text("Type 'token', if you want to see your user token OAuth JWT from Azure Active Directory."));
                    await turnContext.SendActivityAsync(MessageFactory.Text("Type 'logout', if you want to be logged out and cause a new authentication."));
                    await turnContext.SendActivityAsync(MessageFactory.Text("Type 'login' to trigger a new authentication roundtrip."));
                }
                return;
            }
            await turnContext.SendActivityAsync(MessageFactory.Text($"Echo {turnContext.Activity.Text.Length} chars below:"), cancellationToken);
            await turnContext.SendActivityAsync(MessageFactory.Text($"> '{turnContext.Activity.Text}'"), cancellationToken);
        }

        protected override async Task OnMembersAddedAsync(IList<ChannelAccount> membersAdded, ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {
            foreach (ChannelAccount member in membersAdded)
            {
                OAuthPrompt oAuthPrompt = new OAuthPrompt(nameof(OAuthPrompt), oAuthPromptSettings);
                if (member.Id != turnContext.Activity.Recipient.Id)
                {
                    if (string.IsNullOrWhiteSpace(member.Name))
                        await turnContext.SendActivityAsync(MessageFactory.Text("Hello and welcome!"), cancellationToken);
                    else
                        await turnContext.SendActivityAsync(MessageFactory.Text($"Hello and welcome, {member.Name}!"), cancellationToken);
                    TokenResponse tokenResponse = await oAuthPrompt.GetUserTokenAsync(turnContext, cancellationToken);
                    if (tokenResponse == null)
                    {
                        await turnContext.SendActivityAsync(MessageFactory.Text("I need to authenticate you, to see if you are authorized."), cancellationToken);
                        
                        // oAuthPrompt.RunAsync(turnContext, this.ConversationState, cancellationToken)
                        // TO DO: Make sure to save the user's token in the user context/state, because here we're not in a Dialog context.
                    }
                    else
                    {
                        await turnContext.SendActivityAsync(MessageFactory.Text("I have authenticated you recently; you are 'logged in', so to say."), cancellationToken);
                        await turnContext.SendActivityAsync(MessageFactory.Text("Type 'token', if you want to see your user token OAuth JWT from Azure Active Directory."));
                        await turnContext.SendActivityAsync(MessageFactory.Text("Type 'logout', if you want to be logged out and cause a new authentication."));
                        await turnContext.SendActivityAsync(MessageFactory.Text("Type 'login' to trigger a new authentication roundtrip."));
                    }
                }
            }
        }


        private IConfiguration Configuration { get; }
        public ConversationState ConversationState { get; }
        private OAuthPromptSettings oAuthPromptSettings { get; }
    }
}
