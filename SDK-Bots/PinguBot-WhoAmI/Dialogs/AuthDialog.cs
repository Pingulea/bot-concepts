﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;

namespace PinguBotWhoAmI.Dialogs
{
    public class AuthDialog : ComponentDialog
    {
        public AuthDialog(string dialogId, IConfiguration configuration)
            : base(dialogId)
        {
            OAuthUserAuthenticationConnectionName = configuration["OAuth-UserAuthentication-ConnectionName"];
            this.oAuthPromptSettings = new OAuthPromptSettings()
            {
                ConnectionName = OAuthUserAuthenticationConnectionName,
                Text = "Please sign in",
                Title = "Sign In",
                Timeout = 300000, // User has 5 minutes to login (1000 * 60 * 5)
            };
            AddDialog(new OAuthPrompt(nameof(OAuthPrompt), oAuthPromptSettings));
            WaterfallDialog waterfallDialog = new WaterfallDialog(nameof(WaterfallDialog));
            waterfallDialog.AddStep(StartDialogAsync);

            AddDialog(waterfallDialog);
        }

        protected string OAuthUserAuthenticationConnectionName { get; }

        protected async Task<DialogTurnResult> StartDialogAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            OAuthPrompt oAuthPrompt = new OAuthPrompt(nameof(OAuthPrompt), oAuthPromptSettings);
            TokenResponse tokenResponse = await oAuthPrompt.GetUserTokenAsync(stepContext.Context, cancellationToken);
            if (tokenResponse == null)
            {
                // Display the messages: "To use this bot, you must be logged in"
                return await stepContext.BeginDialogAsync(nameof(OAuthPrompt), cancellationToken);
            }
            else
            {
                return await stepContext.EndDialogAsync(cancellationToken: cancellationToken);
            }
        }

        protected async Task<DialogTurnResult> LoginAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            // TO DO: Display the login prompt
            return await stepContext.EndDialogAsync(cancellationToken: cancellationToken);
        }

        protected async Task<DialogTurnResult> DisplayTokenAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            // TO DO: see if we have the token already for the user.
            // If token exists, display the token
            // Else, go to LoginAsync to re-initiate or to StartDialogAsync? Anyway, re-login before displaying
            return await stepContext.EndDialogAsync(cancellationToken: cancellationToken);
        }

        protected override async Task<DialogTurnResult> OnBeginDialogAsync(DialogContext innerDialogContext, object options, CancellationToken cancellationToken = default(CancellationToken))
        {
            DialogTurnResult dialogTurnResult = await InterruptOnLogoutAsync(innerDialogContext, cancellationToken);
            if (dialogTurnResult != null) return dialogTurnResult;
            return await base.OnBeginDialogAsync(innerDialogContext, options, cancellationToken);
        }

        protected override async Task<DialogTurnResult> OnContinueDialogAsync(DialogContext innerDialogContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            DialogTurnResult dialogTurnResult = await InterruptOnLogoutAsync(innerDialogContext, cancellationToken);
            if (dialogTurnResult != null) return dialogTurnResult;
            return await base.OnContinueDialogAsync(innerDialogContext, cancellationToken);
        }

        private async Task<DialogTurnResult> InterruptOnLogoutAsync(DialogContext innerDialogContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (innerDialogContext.Context.Activity.Type == ActivityTypes.Message)
            {
                string text = innerDialogContext.Context.Activity.Text.ToLowerInvariant();
                if (string.Compare(text, "logout", ignoreCase: true) == 0)
                {
                    // The bot adapter encapsulates the authentication processes.
                    BotFrameworkAdapter botAdapter = (BotFrameworkAdapter)innerDialogContext.Context.Adapter;
                    await botAdapter.SignOutUserAsync(innerDialogContext.Context, OAuthUserAuthenticationConnectionName, null, cancellationToken);
                    await innerDialogContext.Context.SendActivityAsync(MessageFactory.Text("You have been signed out."), cancellationToken);
                    return await innerDialogContext.CancelAllDialogsAsync(cancellationToken);
                }
            }
            return null;
        }

        private OAuthPromptSettings oAuthPromptSettings { get; }
    }
}
