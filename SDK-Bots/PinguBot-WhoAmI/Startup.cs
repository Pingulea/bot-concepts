// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Integration.AspNet.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;

using PinguBotWhoAmI.Bots;
using PinguBotWhoAmI.Dialogs;

namespace PinguBotWhoAmI
{
    public class Startup
    {
        private const string BotOpenIdMetadataKey = "BotOpenIdMetadata";

        public Startup(IConfiguration configuration, IWebHostEnvironment hostingEnvironment, ILoggerFactory loggerFactory)
        {
            Configuration = configuration;
            this.m_hostingEnvironment = hostingEnvironment;
            this.m_logger = loggerFactory.CreateLogger(typeof(PinguBotWhoAmI.Startup));
            if (this.m_hostingEnvironment.IsDevelopment())
            {
                this.m_logger.LogInformation($"AzureAd-AppRegistration-ClientID = {this.Configuration["MicrosoftAppId"]}");
                this.m_logger.LogInformation($"AzureAd-AppRegistration-Secret = {this.Configuration["MicrosoftAppPassword"]}");
                this.m_logger.LogInformation($"UserAuthentication-AADConnectionName = {this.Configuration["OAuth-UserAuthentication-ConnectionName"]}");
            }
        }

        public IConfiguration Configuration { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options => options.EnableEndpointRouting = false)
                .SetCompatibilityVersion(CompatibilityVersion.Latest);

            // Create the Bot Framework Adapter.
            services.AddSingleton<IBotFrameworkHttpAdapter, AdapterWithErrorHandler>();

            // Create the storage we'll be using for User and Conversation state. (Memory is great for testing purposes.)
            services.AddSingleton<IStorage, MemoryStorage>();

            // Create the User state. (Used in this bot's Dialog implementation.)
            services.AddSingleton<UserState>();

            // Create the Conversation state. (Used by the Dialog system itself.)
            services.AddSingleton<ConversationState>();

            // The Dialog that will be run by the bot.
            services.AddSingleton<AuthDialog>();

            // Create the bot as a transient. In this case the ASP Controller is expecting an IBot.
            // services.AddTransient<IBot, PinguBotWhoAmI.Bots.NoDialogBot>();
            services.AddTransient<IBot, AuthDialogBot>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder applicationBuilder)
        {
            if (this.m_hostingEnvironment.IsDevelopment())
            {
                applicationBuilder.UseDeveloperExceptionPage();
            }
            else
            {
                applicationBuilder.UseHsts();
            }

            applicationBuilder.UseDefaultFiles();
            applicationBuilder.UseStaticFiles();

            applicationBuilder.UseMvc(routes =>
                {
                    routes.MapRoute(
                        name: "default",
                        template: "{controller=Home}/{action=Index}/{id?}");
                });
        }

        private readonly IWebHostEnvironment m_hostingEnvironment;
        private readonly ILogger m_logger;
    }
}
