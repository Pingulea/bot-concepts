// Re-using database connectivity for a SQL Server, using the Tedious library (http://tediousjs.github.io/tedious)
// Author: Viorel_Alexandru@Hotmail.com

const dotenv = require('dotenv');
const path = require('path');
const Connection = require('tedious').Connection;
const Request = require('tedious').Request;

const ENV_FILE = path.join(__dirname, '.env');
dotenv.config({ path: ENV_FILE });

const connectionString = {
    Server: process.env.SqlConnect_Server, // 'WinDev.Rodax.ro',
    Instance: process.env.SqlConnect_Instance, // 'SqlExpress',
    InitialCatalog: process.env.SqlConnect_Database, // 'BeezMart',
    Username: process.env.SqlConnect_Account, // 'AppAccount.BeezMart',
    Password: process.env.SqlConnect_Password //'Development!123'
};
let connectionConfig = {
    server: connectionString.Server,
    database: connectionString.InitialCatalog,
    options: {
        encrypt: true,
        rowCollectionOnRequestCompletion: true },
    authentication: {
        type: 'default',
        options: {
            userName: connectionString.Username,
            password: connectionString.Password,
            instanceName: connectionString.Instance }
    }
};
let connection = new Connection(connectionConfig);
connection.on('connect', function(err) {
    if (err) {
        console.error('Database connectivity error occured while trying to connect. Error details:');
        console.error(err);
    } else {
        console.info('Connected to SQL Server: ' + connectionString.Server);
    }
});
function processDatabaseQuery(transactSqlCommand, recordProcessingFunction) {
    if (!transactSqlCommand) {
        console.error('The database command is empty; there is nothing to execute or query!');
        return;
    }
    function displayColumn(col) {
        console.info(`\t '${col.metadata.colName}' = '${col.value}'`);
    }
    function callbackAfterRequestExecution(err, rowCount, rows) {
        if (err) {
            console.error('A database command error has occurred while executing the statement below:');
            console.error(transactSqlCommand);
            console.error('Error details:');
            console.error(err);
        } else {
            console.info('T-SQL command result: ' + rowCount + ' rows');
            if (!recordProcessingFunction) {
                console.info('================================================================================');
                console.debug('SqlDatabase.QueryDatabase: no function specified for processing records. Records were simply streamed to info console.');
            }
        }
    }
    let request = new Request(transactSqlCommand, callbackAfterRequestExecution);
    function defaultRecordProcessingFunction(columns) {
        console.info('--------------------------------------------------------------------------------');
        columns.forEach(displayColumn);
    }
    let processRecord = recordProcessingFunction || defaultRecordProcessingFunction;
    request.on('row', processRecord);
    connection.execSql(request);
    // connection.close();
};
function getDatabaseQueryResult(transactSqlCommand, callbackWithResult) {
    if (!transactSqlCommand) {
        console.error('The database command is empty; there is nothing to execute or query!');
        return;
    }
    function callbackAfterRequestExecution(err, rowCount, rows) {
        if (err) {
            console.error('A database command error has occurred while executing the statement below:');
            console.error(transactSqlCommand);
            console.error('Error details:');
            console.error(err);
        } else {
            console.info('T-SQL command result: ' + rowCount + ' rows');
            console.info('Now enumeratig the rows collection: rows.length = ' + rows.length);
            callbackWithResult(rows);
        }
    }
    let request = new Request(transactSqlCommand, callbackAfterRequestExecution);
    connection.execSql(request);
    // connection.close();
}
let publicExports = {
    ConnectionString: connectionString,
    ProcessDatabaseQuery: processDatabaseQuery,
    GetDatabaseQueryResult: getDatabaseQueryResult
};
module.exports.SqlDatabase = publicExports;
