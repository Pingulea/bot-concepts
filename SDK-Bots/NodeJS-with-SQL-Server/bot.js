// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const { ActivityHandler } = require('botbuilder');
const { SqlDatabase } = require('./sql-database.js');

class EchoBot extends ActivityHandler {
    constructor() {
        super();

        let query = ' SELECT TOP 3 Invoice_ID, Issue_Date, Accounting_ID, Customer_Name, Updated_On, Updated_By, ';
        query = query + ' Billing.Invoice_Total_Value(Invoice_ID) AS Total_Value, \'RON\' AS Currency ';
        query = query + ' FROM[Billing].[Invoices]';

        // See https://aka.ms/about-bot-activity-message to learn more about the message and other activity types.
        this.onMessage(async (context, next) => {
            switch (context.activity.text.toLowerCase()) {
            case 'sqlconfig':
                await context.sendActivity('SQL SERVER CONNECTIVITY INFO...');
                await context.sendActivity('SQL Server: ' + SqlDatabase.ConnectionString.Server);
                await context.sendActivity('Instance: ' + SqlDatabase.ConnectionString.Instance);
                await context.sendActivity('Database: ' + SqlDatabase.ConnectionString.InitialCatalog);
                // await context.sendActivity('Username: ' + SqlDatabase.ConnectionString.Username);
                // await context.sendActivity('Password: ' + SqlDatabase.ConnectionString.Password);
                break;
            case 'dbnoquery':
                await context.sendActivity('PERFORMING A DATABASE QUERY DEMO, with no query specified...');
                SqlDatabase.ProcessDatabaseQuery();
                break;
            case 'dbnoprocessing':
                await context.sendActivity('PERFORMING A DATABASE QUERY DEMO, with no function specified to process records...');
                SqlDatabase.ProcessDatabaseQuery(query);
                break;
            case 'database':
                await context.sendActivity('PERFORMING A DATABASE QUERY DEMO...');
                // let rows = SqlDatabase.GetDatabaseQueryResult(query);
                SqlDatabase.GetDatabaseQueryResult(query, async (rows) => {
                    await context.sendActivity.sendActivity('Row count from database: ' + rows.length);
                });

                break;
            default:
                await context.sendActivity(`You said '${context.activity.text}'`);
                break;
            }
            // By calling next() you ensure that the next BotHandler is run.
            await next();
        });

        this.onMembersAdded(async (context, next) => {
            const membersAdded = context.activity.membersAdded;
            for (let cnt = 0; cnt < membersAdded.length; ++cnt) {
                if (membersAdded[cnt].id !== context.activity.recipient.id) {
                    await context.sendActivity('Hello and welcome!');
                }
            }
            // By calling next() you ensure that the next BotHandler is run.
            await next();
        });
    }
}

module.exports.EchoBot = EchoBot;
