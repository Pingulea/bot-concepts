﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace EchoBot.Controllers
{
    public class TestController : Controller
    {
        public static readonly string DIRECT_LINE_SECRET = "kjOVnahWBJI.oxAZWdjj1GOgmt_l1bi0IRKBOndk5JneD5MTTmk8HfA";

        public async Task<IActionResult> Index()
        {
            ChatConfig chatConfig = new ChatConfig();
            using (HttpClient httpClient = new HttpClient())
            {
                using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage())
                {
                    httpRequestMessage.Method = HttpMethod.Post;
                    httpRequestMessage.RequestUri = new Uri("https://directline.botframework.com/v3/directline/tokens/generate");
                    httpRequestMessage.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", DIRECT_LINE_SECRET);
                    string userId = $"dl_{Guid.NewGuid()}";
                    httpRequestMessage.Content = new StringContent(
                    JsonConvert.SerializeObject(
                        new { User = new { Id = userId } }),
                        Encoding.UTF8,
                        "application/json");
                    HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);
                    string token = String.Empty;
                    if (httpResponseMessage.IsSuccessStatusCode)
                    {
                        string httpResponseBody = await httpResponseMessage.Content.ReadAsStringAsync();
                        token = JsonConvert.DeserializeObject<DirectLineToken>(httpResponseBody).token;
                        chatConfig.Token = token;
                        chatConfig.UserId = userId;
                    }
                }
            }
            return View(chatConfig);
        }
    }

    public class DirectLineToken
    {
        public string conversationId { get; set; }
        public string token { get; set; }
        public int expires_in { get; set; }
    }

    public class ChatConfig
    {
        public string Token { get; set; }
        public string UserId { get; set; }
    }
}