// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AdaptiveCards;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using System.IO;

namespace Microsoft.BotBuilderSamples.Bots
{
    public class EchoBot : ActivityHandler
    {
        private string[] delayTriggerringWords = { "Delay", "Latency", "Slow", "Sleep" };
        private string[] errorTriggerringWords = { "Error", "Exception", "Throw", "Break" };
        private string[] adaptiveCardTriggerringWords = { "Adaptive", "Card", "Badge", "AdaptiveCard", "Adaptive-Card" };

        protected override async Task OnMessageActivityAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            /*
             * The bot should greet a new user, as a result of the hidden message from the WebChat client control
            if (turnContext.Activity.ChannelData.Type == "webchat/join")
            {
                //_logger.LogInformation("------- WEBCHAT/JOIN: Conversation started -------");
                //_logger.LogInformation("--> New conversation is about to start, user did not type anything yet. Sending bot introduction...");
                turnContext.SendActivityAsync(MessageFactory.Text("Hello, there! I'm a bot able to answer your questions :-)"), cancellationToken);
            }
            */
            if (errorTriggerringWords.Contains(turnContext.Activity.Text, comparer: System.StringComparer.InvariantCultureIgnoreCase))
            {
                throw new System.ApplicationException($"The Bot has received an exception-triggering word: '{turnContext.Activity.Text}'");
            }
            if (delayTriggerringWords.Contains(turnContext.Activity.Text, comparer: System.StringComparer.InvariantCultureIgnoreCase))
            {
                Thread.Sleep(20000);
                throw new System.ApplicationException($"Bot has received a delay-triggering word: '{turnContext.Activity.Text}'");
            }
            await turnContext.SendActivityAsync(MessageFactory.Text($"Echo {turnContext.Activity.Text.Length} chars: {turnContext.Activity.Text}"), cancellationToken);
            if (adaptiveCardTriggerringWords.Contains(turnContext.Activity.Text, comparer: System.StringComparer.InvariantCultureIgnoreCase))
            {
                string jsonText = await File.ReadAllTextAsync("./Content/adaptive-card.json");
                AdaptiveCardParseResult adaptiveCardParseResult = AdaptiveCard.FromJson(jsonText);
                AdaptiveCard adaptiveCard = adaptiveCardParseResult.Card;
                IList<AdaptiveWarning> adaptiveCardParseWarnings = adaptiveCardParseResult.Warnings;
                if (adaptiveCardParseWarnings.Count > 0)
                {
                    await turnContext.SendActivityAsync(MessageFactory.Text("The adaptive card was parsed, but there are issues:"), cancellationToken);
                    foreach (AdaptiveWarning warning in adaptiveCardParseWarnings)
                    {
                        await turnContext.SendActivityAsync(MessageFactory.Text($"{warning.Code}: {warning.Message}"), cancellationToken);
                    }
                }
                Attachment adaptiveCardAttachment = new Attachment()
                    {
                        ContentType = AdaptiveCard.ContentType,
                        Content = adaptiveCard
                    };
                await turnContext.SendActivityAsync(MessageFactory.Attachment(adaptiveCardAttachment), cancellationToken);
            }
        }

        protected override async Task OnMembersAddedAsync(IList<ChannelAccount> membersAdded, ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {
            foreach (ChannelAccount member in membersAdded)
            {
                if (member.Id != turnContext.Activity.Recipient.Id)
                {
                    await turnContext.SendActivityAsync(MessageFactory.Text($"Hello and welcome!"), cancellationToken);
                }
            }
        }
    }
}
