﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Integration.AspNet.Core;

namespace Microsoft.BotBuilderSamples
{
    // This ASP Controller is created to handle a request. Dependency Injection will provide the Adapter and IBot
    // implementation at runtime. Multiple different IBot implementations running at different endpoints can be
    // achieved by specifying a more specific type for the bot constructor argument.
    [Route("api/messages")]
    [ApiController]
    public class BotController : ControllerBase
    {
        private readonly IBotFrameworkHttpAdapter _adapter;
        private readonly IBot _bot;

        public BotController(IBotFrameworkHttpAdapter adapter, IBot bot)
        {
            _adapter = adapter;
            _bot = bot;
        }

        [HttpPost]
        public async Task PostAsync()
        {
            // Delegate the processing of the HTTP POST to the adapter.
            // The adapter will invoke the bot.
            await _adapter.ProcessAsync(Request, Response, _bot);
        }

        [HttpGet]
        public string Get()
        {
            StringBuilder explanation = new StringBuilder();
            explanation.AppendLine("Hello, there. This is a bot application, that started fine.");
            explanation.AppendLine("It is supposed to only be accessed with HTTP requests having the POST verb (method).");
            explanation.AppendLine("But when a browser is used to navigate to this endpoint, the browser is sending GET requests.");
            explanation.AppendLine("Hence, the response:\n");
            explanation.AppendLine("    --->    405, Method not allowed!");
            Response.StatusCode = 405;
            return explanation.ToString();
        }
    }
}
