﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.BotFramework;
using Microsoft.Bot.Builder.Integration.AspNet.Core;
using Microsoft.Bot.Connector.Authentication;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Microsoft.BotBuilderSamples
{
    public class Startup
    {

        public Startup(IConfiguration configuration, IWebHostEnvironment hostingEnvironment, ILoggerFactory loggerFactory)
        {
            Configuration = configuration;
            this.m_hostingEnvironment = hostingEnvironment;
            this.m_logger = loggerFactory.CreateLogger(typeof(BotBuilderSamples.Startup));
            if (string.Compare(this.m_hostingEnvironment.EnvironmentName, "Development", false) == 0)
            {
                //configurationBuilder.AddUserSecrets("AspNetCore-BeezMart");       // For more details on using the user secret store see http://go.microsoft.com/fwlink/?LinkID=532709
                //configurationBuilder.AddUserSecrets<Startup>();
                this.m_logger.LogInformation($"AzureAd-AppRegistration-ClientID = {this.Configuration["MicrosoftAppId"]}");
                this.m_logger.LogInformation($"AzureAd-AppRegistration-Secret = {this.Configuration["MicrosoftAppPassword"]}");
            }
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson();

            // Add the HttpClientFactory to be used for the QnAMaker calls.
            services.AddHttpClient();

            // Create the credential provider to be used with the Bot Framework Adapter.
            services.AddSingleton<ICredentialProvider, ConfigurationCredentialProvider>();

            // Create the Bot Framework Adapter with error handling enabled. 
            services.AddSingleton<IBotFrameworkHttpAdapter, AdapterWithErrorHandler>();

            // Create the bot as a transient. In this case the ASP Controller is expecting an IBot.
            services.AddTransient<IBot, QnABot>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder applicationBuilder)
        {
            if (this.m_hostingEnvironment.IsDevelopment())
            {
                applicationBuilder.UseDeveloperExceptionPage();
            }
            else
            {
                applicationBuilder.UseHsts();
            }

            applicationBuilder.UseDefaultFiles()
                .UseStaticFiles()
                .UseRouting()
                .UseAuthorization()
                .UseEndpoints(endpoints =>
                    {
                        endpoints.MapControllers();
                    });

            // app.UseHttpsRedirection();
        }

        private readonly IWebHostEnvironment m_hostingEnvironment;
        private readonly Extensions.Logging.ILogger m_logger;
    }
}
