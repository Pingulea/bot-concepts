// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;
using AdaptiveCards;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace MultiInstanceEchoBot.Bots
{
    public class EchoBot : ActivityHandler
    {
        private string[] delayTriggerringWords = { "Delay", "Latency", "Slow", "Sleep" };
        private string[] errorTriggerringWords = { "Error", "Exception", "Throw", "Break", "Crash" };
        private string[] adaptiveCardTriggerringWords = { "Adaptive", "Card", "Badge", "AdaptiveCard", "Adaptive-Card" };
        private string[] helpTriggerringWords = { "Help", "Info", "Assist", "About", "What" };

        public EchoBot(IConfiguration configuration, ILogger<EchoBot> logger)
        {
            this._appConfig = configuration;
            this._logger = logger;
        }

        protected IConfiguration _appConfig { get; }
        protected ILogger<EchoBot> _logger { get; }

        protected override async Task OnMessageActivityAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            ITurnContext<IMessageActivity> tc = turnContext;
            CancellationToken c = cancellationToken;
            if (errorTriggerringWords.Contains(turnContext.Activity.Text, comparer: System.StringComparer.InvariantCultureIgnoreCase))
            {
                await Reply(tc, $"The Bot has received an exception-triggering word...", c);
                throw new System.ApplicationException($"The Bot has received an exception-triggering word: '{turnContext.Activity.Text}'");
            }
            if (delayTriggerringWords.Contains(turnContext.Activity.Text, comparer: System.StringComparer.InvariantCultureIgnoreCase))
            {
                await Reply(tc, $"Bot has received a delay-triggering word...", c);
                Thread.Sleep(20000);
            }
            switch (turnContext.Activity.Text.ToLower())
            {
                case "type":
                    await Reply(tc, "BOT TYPE (cross-instance): " + _appConfig.GetSection("BotType").Value, c);
                    await Reply(tc, "BOT DESCRIPTION (cross-instance): " + _appConfig.GetSection("BotDescription").Value, c);
                    break;
                case "name":
                    await Reply(tc, "BOT NAME (instance-specific): " + _appConfig.GetSection("BotInstance").Value, c);
                    break;
                case "appid":
                    await Reply(tc, "BOT AZURE APP ID (instance-specific): " + _appConfig.GetSection("MicrosoftAppId").Value, c);
                    break;
                case "dependency":
                    await Reply(tc, "DEPENDENCIES (instance-specific): " + _appConfig.GetSection("DependencySetting").Value, c);
                    break;
                case "link":
                    await Reply(tc, "Go to http://botframework.com and follow [Documentation](https://aka.ms/bot-framework-www-portal-docs) link.", c);
                    await Reply(tc, "You may also want to use the <https://aka.ms/bot-framework-www-portal-emulator|Emulator> or read the <https://blog.botframework.com|Blog>", c);
                    await Reply(tc, "http://botframework.com", c);
                    break;
                case "version":
                    await Reply(tc, "Oh, my version is v2.3.4", c);
                    break;
                default:
                    await Reply(tc, $"Echo {turnContext.Activity.Text.Length} chars below:", c);
                    await Reply(tc, $"> '{turnContext.Activity.Text}'", c);
                    break;
            }
            if (adaptiveCardTriggerringWords.Contains(turnContext.Activity.Text, comparer: System.StringComparer.InvariantCultureIgnoreCase))
            {
                string jsonText = await File.ReadAllTextAsync("./Content/adaptive-card.json");
                AdaptiveCardParseResult adaptiveCardParseResult = AdaptiveCard.FromJson(jsonText);
                AdaptiveCard adaptiveCard = adaptiveCardParseResult.Card;
                IList<AdaptiveWarning> adaptiveCardParseWarnings = adaptiveCardParseResult.Warnings;
                if (adaptiveCardParseWarnings.Count > 0)
                {
                    await Reply(tc, "The adaptive card was parsed, but there are issues:", c);
                    foreach (AdaptiveWarning warning in adaptiveCardParseWarnings)
                    {
                        await Reply(tc, $"Warning {warning.Code}: {warning.Message}", c);
                    }
                }
                Attachment adaptiveCardAttachment = new Attachment()
                {
                    ContentType = AdaptiveCard.ContentType,
                    Content = adaptiveCard
                };
                await turnContext.SendActivityAsync(MessageFactory.Attachment(adaptiveCardAttachment), cancellationToken: cancellationToken);
            }
            if (helpTriggerringWords.Contains(turnContext.Activity.Text, comparer: System.StringComparer.InvariantCultureIgnoreCase))
            {
                await Reply(tc, $"Type 'Delay', 'Latency', 'Slow', or 'Sleep' to trigger a delay.", c);
                await Reply(tc, $"Type 'Error', 'Exception', 'Throw', 'Break', or 'Crash' to trigger an exception.", c);
                await Reply(tc, $"Type 'Adaptive', 'Card', 'Badge', 'AdaptiveCard', or 'Adaptive-Card' to get an adaptive card.", c);
                await Reply(tc, $"Type 'Help', 'Info', 'Assist', 'About', or 'What' to get the keywords.", c);
            }
        }

        protected async Task Reply(ITurnContext<IMessageActivity> turnContext, string text, CancellationToken cancellationToken)
        {
            await turnContext.SendActivityAsync(MessageFactory.Text(text), cancellationToken);
        }

        protected override async Task OnMembersAddedAsync(IList<ChannelAccount> membersAdded, ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {
            // The Web Chat and Direct Line channel may be sending the Welcome/Greeting message differently:
            // https://github.com/microsoft/BotFramework-WebChat/blob/master/docs/WELCOME_MESSAGE.md#secrets-and-tokens-without-user-ids
            if (string.Compare(turnContext.Activity.ChannelId, "DirectLine", ignoreCase: true) == 0) return;
            // if (string.Compare(turnContext.Activity.ChannelId, "WebChat", ignoreCase: true) == 0) return;
            foreach (ChannelAccount member in membersAdded)
            {
                if (member.Id != turnContext.Activity.Recipient.Id)
                {
                    string nameOfUser = string.IsNullOrWhiteSpace(member.Name) ? "dear user" : member.Name;
                    await turnContext.SendActivityAsync(MessageFactory.Text($"Hello and welcome, {nameOfUser}!"), cancellationToken: cancellationToken);
                    await turnContext.SendActivityAsync(MessageFactory.Text($"I'm a simple message echoing bot, demonstrating latency, exception throwing and welcoming on '{turnContext.Activity.ChannelId}' channel."), cancellationToken: cancellationToken);
                    await turnContext.SendActivityAsync(MessageFactory.Text("Type 'Help' for keywords..."), cancellationToken: cancellationToken);
                }
            }
        }

        protected override async Task OnEventAsync(ITurnContext<IEventActivity> turnContext, CancellationToken cancellationToken)
        {
            if (turnContext.Activity.Name == "webchat/join")
            {
                await turnContext.SendActivityAsync(MessageFactory.Text($"Hello and welcome, on the '{turnContext.Activity.ChannelId}' channel.!"), cancellationToken: cancellationToken);
                await turnContext.SendActivityAsync(MessageFactory.Text($"I'm a simple message echoing bot, demonstrating latency, exception throwing and welcoming. Type 'Help' for keywords..."), cancellationToken: cancellationToken);
            }
        }
        public override async Task OnTurnAsync(ITurnContext turnContext, CancellationToken cancellationToken = default)
        {
            this._logger.LogDebug("turnContext.Activity.Text... " + turnContext.Activity.Text);
            await base.OnTurnAsync(turnContext, cancellationToken: cancellationToken);
        }
    }
}
