﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace MultiInstanceEchoBot.Controllers
{
    [Route("directline-token")]
    [ApiController]
    public class TokenController : Controller
    {
        [HttpPost]
        public async Task<ChatConfig> PostAsync()
        {
            string directLineSecret = appConfig["DirectLineSecret"];
            if (string.IsNullOrWhiteSpace(directLineSecret)) throw new MissingFieldException("App configuration is missing the 'DirectLineSecret' field.");
            ChatConfig chatConfig = new ChatConfig();
            using (HttpClient httpClient = new HttpClient())
            {
                using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage())
                {
                    httpRequestMessage.Method = HttpMethod.Post;
                    httpRequestMessage.RequestUri = new Uri("https://directline.botframework.com/v3/directline/tokens/generate");
                    httpRequestMessage.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", directLineSecret);
                    string userId = $"dl_{Guid.NewGuid()}";
                    httpRequestMessage.Content = new StringContent(
                    JsonConvert.SerializeObject(
                        new { User = new { Id = userId } }),
                        Encoding.UTF8,
                        "application/json");
                    HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);
                    string token = String.Empty;
                    if (httpResponseMessage.IsSuccessStatusCode)
                    {
                        string httpResponseBody = await httpResponseMessage.Content.ReadAsStringAsync();
                        token = JsonConvert.DeserializeObject<DirectLineToken>(httpResponseBody).token;
                        chatConfig.Token = token;
                        chatConfig.UserId = userId;
                    }
                    else
                    {
                        throw new HttpRequestException("Could not contact secret token service at https://directline.botframework.com/v3/directline/tokens/generate");
                    }
                }
            }
            return chatConfig;
        }

        public TokenController(IConfiguration configuration)
        {
            this.appConfig = configuration;
        }
        protected IConfiguration appConfig { get; }

    }

    public class DirectLineToken
    {
        public string conversationId { get; set; }
        public string token { get; set; }
        public int expires_in { get; set; }
    }

    public class ChatConfig
    {
        public string Token { get; set; }
        public string UserId { get; set; }
    }
}
