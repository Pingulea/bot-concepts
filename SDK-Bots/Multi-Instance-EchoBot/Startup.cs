// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Integration.AspNet.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;

using MultiInstanceEchoBot.Bots;

namespace MultiInstanceEchoBot
{
    public class Startup
    {
        private const string BotOpenIdMetadataKey = "BotOpenIdMetadata";

        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration, IWebHostEnvironment hostingEnvironment)
        {
            Configuration = configuration;
            this.m_hostingEnvironment = hostingEnvironment;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc( options => options.EnableEndpointRouting = false );

            // Create the Bot Framework Adapter.
            services.AddSingleton<IBotFrameworkHttpAdapter, AdapterWithErrorHandler>();

            // Create the bot as a transient. In this case the ASP Controller is expecting an IBot.
            services.AddTransient<IBot, EchoBot>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder applicationBuilder, ILogger<Startup> logger)
        {
            this.m_logger = logger;
            if (this.m_hostingEnvironment.IsDevelopment())
            {
                this.m_logger.LogInformation($"AzureAd-AppRegistration-ClientID = {this.Configuration["MicrosoftAppId"]}");
                this.m_logger.LogInformation($"AzureAd-AppRegistration-Secret = {this.Configuration["MicrosoftAppPassword"]}");
            }

            if (this.m_hostingEnvironment.IsDevelopment())
            {
                applicationBuilder.UseDeveloperExceptionPage();
            }
            else
            {
                applicationBuilder.UseHsts();
            }

            applicationBuilder.UseDefaultFiles();
            applicationBuilder.UseStaticFiles();
            // applicationBuilder.UseNamedPipes(System.Environment.GetEnvironmentVariable("APPSETTING_WEBSITE_SITE_NAME") + ".directline");
            applicationBuilder.UseWebSockets();
            // applicationBuilder.UseRouting();
            applicationBuilder.UseAuthorization();
            //applicationBuilder.UseEndpoints(endpoints =>
            //    {
            //        endpoints.MapControllers();
            //    });
            // applicationBuilder.UseHttpsRedirection();
            applicationBuilder.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private readonly IWebHostEnvironment m_hostingEnvironment;
        private ILogger<Startup> m_logger;
    }
}
