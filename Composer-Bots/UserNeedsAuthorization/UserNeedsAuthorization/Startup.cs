using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Bot.Builder.Dialogs.Adaptive.Runtime.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RU_BOT.UserAuthorization.Repository;
using Microsoft.Extensions.Logging;

namespace RU_BOT
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public ILogger<Startup> Logger { get; set; }
        protected string[] RequiredSettings = {
                "MicrosoftAppId",
                "MicrosoftAppPassword",
                "MyBotSettings:BackendApiCallKey",
                "MyBotSettings:BotAppApiCallKey",
                "MyBotSettings:BotAppApiCallUrl",
                "MyBotSettings:HandleName",
                "MyBotSettings:WebChatSecretKey",
                "MyBotSettings:DirectLineSecretKey",
                "ConnectionStrings:UserAuthDatabase"
            };

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson();
            services.AddRazorPages();
            services.AddBotRuntime(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder appBuilder, IWebHostEnvironment hostingEnvironment, ILogger<Startup> logger)
        {
            this.Logger = logger;
            if (hostingEnvironment.IsDevelopment())
            {
                appBuilder.UseDeveloperExceptionPage();
                LogStartupConfigSettings(revealFullSettingValue: true);
            }
            else
            {
                LogStartupConfigSettings(revealFullSettingValue: false);
            }
            DbInfo.ConnectionString = this.Configuration["ConnectionStrings:UserAuthDatabase"];
            
            appBuilder.UseDefaultFiles();

            // Set up custom content types - associating file extension to MIME type.
            var provider = new FileExtensionContentTypeProvider();
            provider.Mappings[".lu"] = "application/vnd.microsoft.lu";
            provider.Mappings[".qna"] = "application/vnd.microsoft.qna";

            // Expose static files in manifests folder for skill scenarios.
            appBuilder.UseStaticFiles(new StaticFileOptions
            {
                ContentTypeProvider = provider
            });
            appBuilder.UseWebSockets();
            appBuilder.UseRouting();
            appBuilder.UseAuthorization();
            appBuilder.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapRazorPages();
            });
        }
        public void LogStartupConfigSettings(bool revealFullSettingValue = false)
        {
            this.Logger.LogInformation("Logging the required expected settings:...");
            string settingValue = null;
            foreach (string settingName in this.RequiredSettings)
            {
                settingValue = Configuration[settingName];
                if (string.IsNullOrWhiteSpace(settingValue))
                {
                    this.Logger.LogError($"\t>\tERRORS are expected because required setting '{settingValue}' is missing, not populated!");
                    settingValue = "[NULL]";
                }
                else
                {
                    if (revealFullSettingValue)
                    {
                        this.Logger.LogInformation($"\t>\t{settingName}\t=\t{settingValue}");
                    }
                    else
                    {
                        if (settingName.Length >= 7)
                        {
                            settingValue = settingValue.Substring(0, 7) + "***(redacted)***; TotalLength=" + settingValue.Length + " chars.";
                        }
                        else
                        {
                            settingValue = "***(redacted)***; TotalLength=" + settingValue.Length + " chars";
                        }   
                    }
                }
            }
        }
    }
}
