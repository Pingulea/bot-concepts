﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Text.Json;
using System.Threading.Tasks;

namespace RU_BOT.Code
{
    [AttributeUsage(validOn: AttributeTargets.Class)]
    public class ApiKeyAttribute : Attribute, IAsyncActionFilter
    {
        private const string ApiKey_Header_Name = "X-API-Key";
        private const string ApiKey_Config_Setting_Name = "MyBotSettings:BotAppApiCallKey";

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            ProblemDetails problem;
            if (!context.HttpContext.Request.Headers.TryGetValue(ApiKey_Header_Name, out var apiKeyFromCallRequest))
            {
                problem = new ProblemDetails();
                problem.Status = 401;
                problem.Title = "Unauthorized, missing API key";
                problem.Detail = $"A call has been made to '{context.HttpContext.Request.Path}' without providing an API call key in request's header 'X-API-Key'.";
                context.Result = new ContentResult()
                {
                    StatusCode = problem.Status,
                    Content = JsonSerializer.Serialize(problem)
                };
                return;
            }
            IConfiguration appSettings = context.HttpContext.RequestServices.GetRequiredService<IConfiguration>();
            string configuredApiKey = appSettings.GetValue<string>(ApiKey_Config_Setting_Name);
            if (!configuredApiKey.Equals(apiKeyFromCallRequest))
            {
                problem = new ProblemDetails();
                problem.Status = 401;
                problem.Title = "Unauthorized, wrong API key";
                problem.Detail = $"A call has been made to '{context.HttpContext.Request.Path}' providing an invalid API call key in request's header 'X-API-Key'.";
                context.Result = new ContentResult()
                {
                    StatusCode = problem.Status,
                    Content = JsonSerializer.Serialize(problem)
                };
                return;
            }
            await next();
        }
    }
}
