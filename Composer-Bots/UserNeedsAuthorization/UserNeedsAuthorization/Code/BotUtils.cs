﻿using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace RU_BOT.Code
{
    public static class BotUtils
    {
        public static async Task<string> GetEphemeralToken(string WebChatOrDirectLineSecret)
        {
            if (string.IsNullOrWhiteSpace(WebChatOrDirectLineSecret)) throw new ArgumentException("The DirectLine or WebChat secret is not passed properly");
            string ephemeralToken = null;
            using (HttpClient httpClient = new HttpClient())
            {
                using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage())
                {
                    httpRequestMessage.Method = HttpMethod.Post;
                    httpRequestMessage.RequestUri = new Uri("https://directline.botframework.com/v3/directline/tokens/generate");
                    httpRequestMessage.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", WebChatOrDirectLineSecret);
                    string userId = $"dl_{Guid.NewGuid()}";
                    httpRequestMessage.Content = new StringContent(
                    JsonSerializer.Serialize(
                        new { User = new { Id = userId } }),
                        Encoding.UTF8,
                        "application/json");
                    HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);
                    string token = String.Empty;
                    if (httpResponseMessage.IsSuccessStatusCode)
                    {
                        string httpResponseBody = await httpResponseMessage.Content.ReadAsStringAsync();
                        ephemeralToken = JsonSerializer.Deserialize<DirectLineToken>(httpResponseBody).token;
                    }
                    else
                    {
                        throw new HttpRequestException("Could not contact secret token service at https://directline.botframework.com/v3/directline/tokens/generate");
                    }
                }
            }
            return ephemeralToken;
        }
    }
}
