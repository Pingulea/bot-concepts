﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using RU_BOT.Code;
using RU_BOT.UserAuthorization.Models;
using RU_BOT.UserAuthorization.Repository;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//  For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860
//  To protect API by using calls with API key:
//      http://codingsonata.com/secure-asp-net-core-web-api-using-api-key-authentication/
//      https://www.domstamand.com/securing-asp-net-core-webapi-with-an-api-key/

namespace RU_BOT.Controllers
{
    [Route("api/user-authorization")]
    [ApiController]
    [ApiKey]
    public class UserAuthorizationController : ControllerBase
    {
        public ILogger<UserAuthorizationController> Logger { get; set; }

        public UserAuthorizationController(ILogger<UserAuthorizationController> logger)
        {
            this.Logger = logger;
        }

        [HttpGet]       // GET: api/user-authorization/get-recent-users?maxResults=10
        [Route("get-recent-users")]
        public async Task<List<BotUserModel>> GetRecentUsers(short maxResults = 100)
        {
            Logger.LogDebug("Controller action called: UserAuthorizationController.GetRecentUsers(...).");
            Logger.LogDebug($"Calling BotUsers.GetRecentlyAddedAsync(MaxRecordsCount: {maxResults})...");
            List<BotUserModel> botUsers = await BotUsers.GetRecentlyAddedAsync(MaxRecordsCount: maxResults);
            Logger.LogDebug($"Records count retrieved by BotUsers.GetRecentlyAddedAsync(MaxRecordsCount: {maxResults}): {botUsers.Count}.");
            return botUsers;
        }

        /// <summary>
        ///     <para>Gets the user by (Authority & Email), if it is recorded in DB as BotUser.</para>
        /// </summary>
        /// <param name="authority"></param>
        /// <param name="email"></param>
        /// <param name="subjectId"></param>
        /// <returns>A BotUser record or a 404 response, if the user record does not exist in DB.</returns>
        [HttpGet]       // GET api/user-authorization/get-user/google/john.doe@google.com
        [Route("get-user/{authority}/{email}")]
        public async Task<ActionResult> GetUser(string authority, string email, string subjectId)
        {
            Logger.LogDebug("Controller action called: UserAuthorizationController.GetUser(...).");
            ProblemDetails problem;
            Logger.LogDebug($"Calling BotUsers.GetByEmailAsync(Authority: '{authority}', Email: '{email}', SubjectID: '{subjectId}')...");
            BotUserModel botUser = await BotUsers.GetByEmailAsync(Authority: authority, Email: email, SubjectID: subjectId);
            if (botUser == null)
            {
                problem = new ProblemDetails();
                problem.Status = 404;
                problem.Title = "Bot user doesn't exist";
                problem.Detail = $"No BotUser record was found having Authority='{authority}' AND... Email='{email}' (OR SubjectId='{subjectId}').";
                Logger.LogDebug($"No BotUser record was found by BotUsers.GetByEmailAsync(Authority: '{authority}', Email: '{email}', SubjectID: '{subjectId}').");
                return NotFound(problem);
            }
            Logger.LogDebug($"User found. Authority: '{authority}', Email: '{email}', SubjectID: '{subjectId}'");
            return Ok(botUser);
        }

        /// <summary>
        ///     <para>Ensures that a Bot User record exists in the DB for the {Authority, Email, SubjectID} touple.</para>
        ///     <para>If the DB record exists, it is returned. If not, it is created and the new record is returned.</para>
        /// </summary>
        /// <param name="botUser"></param>
        /// <returns>The Bot User record from DB for the {Authority, Email, SubjectID} touple</returns>
        [HttpPost]      // POST api/user-authorization/user-record
        [Route("user-record")]
        public async Task<ActionResult> GetOrCreateUser([FromBody] BotUserModel botUser)
        {
            Logger.LogDebug("Controller action called: UserAuthorizationController.GetOrCerateUser(...).");
            ProblemDetails problem;
            Logger.LogDebug($"Calling BotUsers.GetByEmailAsync(Authority: '{botUser.Authority}', Email: '{botUser.Email}', SubjectID: '{botUser.SubjectId}')...");
            BotUserModel existingBotUser = await BotUsers.GetByEmailAsync(Authority: botUser.Authority, Email: botUser.Email, SubjectID: botUser.SubjectId);
            if (existingBotUser == null)
            {
                Logger.LogDebug("A BotUSer record does not exist in DB; so now creating one...");
                Logger.LogDebug($"Preparing to call BotUsers.SaveAsync(Authority: {botUser.Authority}, Email: {botUser.Email}, SubjectID: {botUser.SubjectId})...");
                #region validate the input
                List<string> missingFields = new List<string>();
                {
                    if (string.IsNullOrWhiteSpace(botUser.Authority)) missingFields.Add("Authority");
                    if (string.IsNullOrWhiteSpace(botUser.Email)) missingFields.Add("Email");
                    if (string.IsNullOrWhiteSpace(botUser.SubjectId)) missingFields.Add("SubjectId");
                }
                if (missingFields.Count > 0)
                {
                    problem = new ProblemDetails();
                    problem.Status = 400;
                    problem.Title = "Missing fields for bot user";
                    StringBuilder stb = new StringBuilder();
                    foreach (string field in missingFields) stb.Append($"'{field}', ");
                    problem.Detail = "The submitted records is missing one or more required field(s): " + stb.ToString().Substring(0, stb.Length - 2) + ".";
                    Logger.LogDebug(problem.Detail);
                    return StatusCode(problem.Status.Value, problem);
                }
                #endregion
                botUser.LastUpdate = DateTime.Now;
                botUser.AddedOn = DateTime.Now;
                bool success = await BotUsers.SaveAsync(botUser);
                if (success)
                {
                    Logger.LogInformation($"Call to BotUsers.SaveAsync(), with Authority={botUser.Authority} & Email={botUser.Email}, resulted in record with User ID = {botUser.Id}");
                    return Ok(botUser);
                }
                else
                {
                    problem = new ProblemDetails();
                    problem.Status = 500;
                    problem.Title = "Could not add user";
                    problem.Detail = $"A BotUser record could not be persisted in the DB repository.";
                    Logger.LogError(problem.Detail);
                    return StatusCode(problem.Status.Value, problem);
                }
            }
            else
            {
                Logger.LogDebug($"User record ID {existingBotUser.Id} available. Authority: '{botUser.Authority}', Email: '{botUser.Email}', SubjectID: '{botUser.SubjectId}'");
                return Ok(existingBotUser);
            }
        }

        [HttpPost]      // POST api/user-authorization/add-user
        [Route("add-user")]
        public async Task<ActionResult> AddUser([FromBody] BotUserModel botUser)
        {
            Logger.LogDebug("Controller action called: UserAuthorizationController.AddUser(...).");
            Logger.LogDebug($"Preparing to call BotUsers.SaveAsync(Authority: {botUser.Authority}, Email: {botUser.Email}, SubjectID: {botUser.SubjectId})...");
            ProblemDetails problem;
            #region validate the input
            List<string> missingFields = new List<string>();
            {
                if (string.IsNullOrWhiteSpace(botUser.Authority)) missingFields.Add("Authority");
                if (string.IsNullOrWhiteSpace(botUser.Email)) missingFields.Add("Email");
                if (string.IsNullOrWhiteSpace(botUser.SubjectId)) missingFields.Add("SubjectId");
            }
            if (missingFields.Count > 0)
            {
                problem = new ProblemDetails();
                problem.Status = 400;
                problem.Title = "Missing fields for bot user";
                StringBuilder stb = new StringBuilder();
                foreach (string field in missingFields) stb.Append($"'{field}', ");
                problem.Detail = "The submitted records is missing one or more required field(s): " + stb.ToString().Substring(0, stb.Length - 2) + ".";
                Logger.LogDebug(problem.Detail);
                return StatusCode(problem.Status.Value, problem);
            }
            #endregion
            Logger.LogDebug($"Calling BotUsers.GetByEmailAsync(Authority: '{botUser.Authority}', Email: '{botUser.Email}', SubjectID: '{botUser.SubjectId}')...");
            BotUserModel existingBotUser = await BotUsers.GetByEmailAsync(Authority: botUser.Authority, Email: botUser.Email, SubjectID: botUser.SubjectId);
            if (existingBotUser != null)
            {
                problem = new ProblemDetails();
                problem.Status = 409;
                problem.Title = "Bot user exists";
                problem.Detail = $"The specified BotUser record already exists in the DB repository, having ID={existingBotUser.Id}.";
                Logger.LogInformation(problem.Detail);
                return Conflict(problem);
            }
            Logger.LogInformation($"Call to BotUsers.GetByEmailAsync(Authority: '{botUser.Authority}', Email: '{botUser.Email}', SubjectID: '{botUser.SubjectId}') returned no user record; hence, a new user record is being created.");
            bool success = await BotUsers.SaveAsync(botUser);
            if (success)
            {
                string botUserLocation = $"/api/user-authorization/get-user/{botUser.Authority}/{botUser.Email}";
                BotUserModel createdBotUser = await BotUsers.GetByEmailAsync(Authority: botUser.Authority, Email: botUser.Email);
                Logger.LogInformation($"Call to BotUsers.GetByEmailAsync(Authority: {botUser.Authority}, Email: {botUser.Email}) resulted in record with User ID = {botUser.Id}");
                return Created(botUserLocation, createdBotUser);
            }
            else
            {
                problem = new ProblemDetails();
                problem.Status = 500;
                problem.Title = "Could not add user";
                problem.Detail = $"A BotUser record could not be persisted in the DB repository.";
                Logger.LogError(problem.Detail);
                return StatusCode(problem.Status.Value, problem);
            }
        }

        [HttpPut]       // PUT api/user-authorization/update-user
        [Route("update-user")]
        public async Task<ActionResult> UpdateUser([FromBody] BotUserModel botUser)
        {
            Logger.LogDebug($"Controller action called: UserAuthorizationController.UpdateUser(...).");
            ProblemDetails problem;
            Logger.LogDebug($"Calling BotUsers.GetByEmailAsync(Authority: '{botUser.Authority}', Email: '{botUser.Email}', SubjectID: '{botUser.SubjectId}')...");
            BotUserModel existingBotUser = await BotUsers.GetByEmailAsync(Authority: botUser.Authority, Email: botUser.Email, SubjectID: botUser.SubjectId);
            if (existingBotUser == null)
            {
                problem = new ProblemDetails();
                problem.Status = 404;
                problem.Title = "Bot user doesn't exist";
                problem.Detail = $"No BotUser record was found having Authority='{botUser.Authority}' AND... Email='{botUser.Email}' (OR SubjectId='{botUser.SubjectId}').";
                Logger.LogInformation(problem.Detail);
                return NotFound(problem);
            }
            Logger.LogInformation($"Call to BotUsers.GetByEmailAsync(Authority: '{botUser.Authority}', Email: '{botUser.Email}', SubjectID: '{botUser.SubjectId}') returned record with User ID = {existingBotUser.Id}. Now updating...");
            bool success = await BotUsers.SaveAsync(botUser);
            if (success)
            {
                string botUserLocation = $"/api/user-authorization/get-user/{botUser.Authority}/{botUser.Email}";
                BotUserModel updatedBotUser = await BotUsers.GetByEmailAsync(botUser.Authority, botUser.Email);
                Logger.LogInformation($"Call to BotUsers.GetByEmailAsync(Authority: {botUser.Authority}, Email: {botUser.Email}) resulted in (updated) record with User ID = {botUser.Id}");
                return Accepted(botUserLocation, updatedBotUser);
            }
            else
            {
                problem = new ProblemDetails();
                problem.Status = 500;
                problem.Title = "Could not add user";
                problem.Detail = $"A BotUser record could not be persisted in the DB repository.";
                Logger.LogError(problem.Detail);
                return StatusCode(problem.Status.Value, problem);
            }
        }

        [HttpDelete]    // DELETE api/user-authorization/delete-user?userId=7
        [Route("delete-user")]
        public ActionResult DeleteUser(int userId)
        {
            Logger.LogDebug($"Controller action called: UserAuthorizationController.DeleteUser(...).");
            // throw new NotImplementedException();
            ProblemDetails problem = new ProblemDetails();
            problem.Status = 405;
            problem.Title = "Method not allowed";
            problem.Detail = $"The method for deleting bot users is not implemented. BotUser records can't be deleted, currently.";
            Logger.LogWarning($"A call has been made attempting to delete BotUser record with User ID = {userId}. This method is not implemented; response 405 is being returned for the call.");
            return Problem(detail: problem.Detail, instance: null, statusCode: problem.Status, title: problem.Title, type: null);
        }

        [HttpPost]      // POST api/user-authorization/create-voucher
        [Route("create-voucher")]
        public async Task<ActionResult> AddVoucher([FromBody] VoucherModel newVoucher)
        {
            Logger.LogDebug($"Controller action called: UserAuthorizationController.AddVoucher(...).");
            ProblemDetails problem;
            if (string.IsNullOrWhiteSpace(newVoucher.Code))
            {
                Logger.LogInformation("The newVoucher.Code is null or whitespace. Replacing with a new GUID.");
                newVoucher.Code = Guid.NewGuid().ToString().ToUpper();
            }
            if (!newVoucher.CreatedOn.HasValue) newVoucher.CreatedOn = DateTime.Now;
            Logger.LogDebug($"... newVoucher.Code = '{newVoucher.Code}'");
            Logger.LogDebug($"... newVoucher.ValidityPeriod = '{newVoucher.ValidityPeriod}'");
            Logger.LogDebug($"... newVoucher.ExpiresOn = '{newVoucher.ExpiresOn}'");
            Logger.LogDebug($"Calling Vouchers.RetrieveAsync(voucherCode: {newVoucher.Code}) to check if Voucher record already exists.");
            VoucherModel existingVoucher = await Vouchers.RetrieveAsync(voucherCode: newVoucher.Code);
            if (existingVoucher != null)
            {
                problem = new ProblemDetails();
                problem.Status = 409;
                problem.Title = "Bot voucher exists";
                problem.Detail = $"The specified Voucher record already exists in the DB repository for Code='{newVoucher.Code}', having ID={existingVoucher.Id}.";
                Logger.LogInformation(problem.Detail);
                return Conflict(problem);
            }
            Logger.LogDebug($"Calling Vouchers.SaveAsync(voucher: newVoucher)...");
            bool success = await Vouchers.SaveAsync(newVoucher);
            if (success)
            {
                string voucherLocation = $"/api/user-authorization/get-voucher-record/{newVoucher.Id}";
                VoucherModel createdVoucher = await Vouchers.RetrieveAsync(newVoucher.Id.Value);
                Logger.LogInformation($"Voucher record created for Voucher Code '{newVoucher.Code}', having Voucher ID={newVoucher.Id} and validity of {newVoucher.ValidityPeriod} days.");
                return Created(voucherLocation, createdVoucher);
            }
            else
            {
                problem = new ProblemDetails();
                problem.Status = 500;
                problem.Title = "Could not add voucher";
                problem.Detail = $"A Voucher record could not be persisted in the DB repository.";
                Logger.LogError(problem.Detail);
                return StatusCode(problem.Status.Value, problem);
            }
        }

        [HttpGet]       // GET api/user-authorization/get-voucher-record/23456
        [Route("get-voucher-record/{id}")]
        public async Task<ActionResult<VoucherModel>> GetVoucher(int id)
        {
            Logger.LogDebug($"Controller action called: UserAuthorizationController.GetVoucher(...).");
            ProblemDetails problem;
            Logger.LogDebug($"Calling Vouchers.RetrieveAsync(id: '{id}')...");
            VoucherModel voucher = await Vouchers.RetrieveAsync(id);
            if (voucher == null)
            {
                problem = new ProblemDetails();
                problem.Status = 404;
                problem.Title = "Voucher doesn't exist";
                problem.Detail = $"No Voucher record was found having Id={id}.";
                Logger.LogInformation(problem.Detail);
                return NotFound(problem);
            }
            Logger.LogInformation($"Calling Vouchers.RetrieveAsync(id: '{id}') returned Voucher Code={voucher.Code}.");
            return voucher;
        }

        /// <summary>
        ///     <para>Returns a voucher records after performing validation:</para>
        ///     <para>If the voucher is expired or already assigned, then no voucher is returned.</para>
        /// </summary>
        /// <param name="code"></param>
        /// <returns>A Voucher record or 404/409 if voucher doed not exist or cannot be used.</returns>
        [HttpGet]       // GET api/user-authorization/get-voucher/QWERT-ASDFG-ZXCVB
        [Route("get-voucher/{code}")]
        public async Task<ActionResult<VoucherModel>> GetVoucher(string code)
        {
            Logger.LogDebug($"Controller action called: UserAuthorizationController.GetVoucher(...).");
            ProblemDetails problem;
            Logger.LogDebug($"Calling Vouchers.RetrieveAsync(code: '{code}')...");
            VoucherModel voucher = await Vouchers.RetrieveAsync(code);
            if (voucher == null)
            {
                problem = new ProblemDetails();
                problem.Status = 404;
                problem.Title = "Voucher doesn't exist";
                problem.Detail = $"No Voucher record was found having Id='{code}'.";
                Logger.LogInformation(problem.Detail);
                return NotFound(problem);
            }
            if (voucher.ExpiresOn.HasValue && voucher.ExpiresOn.Value < DateTime.Now)
            {
                problem = new ProblemDetails();
                problem.Status = 409;
                problem.Title = "Voucher is expired";
                problem.Detail = $"The voucher '{code}' has an expiration date which passed; it can no longer be used.";
                Logger.LogInformation(problem.Detail);
                return Conflict(problem);
            }
            if (voucher.AssignedOn.HasValue)
            {
                problem = new ProblemDetails();
                problem.Status = 409;
                problem.Title = "Voucher already used";
                problem.Detail = $"The voucher '{code}' is already assigned and it cannot be used more than once.";
                Logger.LogInformation(problem.Detail);
                return Conflict(problem);
            }
            Logger.LogInformation($"Calling Vouchers.RetrieveAsync(code: '{code}') returned Voucher ID={voucher.Id}.");
            return voucher;
        }

        [HttpGet]       // GET api/user-authorization/get-recent-vouchers?maxResults=10&excludeAssigned=true
        [Route("get-recent-vouchers")]
        public async Task<List<VoucherModel>> GetRecentVouchersAsync(int maxResults = 10, bool excludeAssigned = true)
        {
            Logger.LogDebug($"Controller action called: UserAuthorizationController.GetRecentVouchersAsync(...).");
            Logger.LogDebug($"Calling Vouchers.GetRecentAsync(maxRecordsCount: '{maxResults}')...");
            List<VoucherModel> voucherList = await Vouchers.GetRecentAsync(maxRecordsCount: maxResults, excludeAssigned: excludeAssigned);
            Logger.LogInformation($"Call to Vouchers.GetRecentAsync(maxRecordsCount: '{maxResults}') returned {voucherList.Count} results.");
            return voucherList;
        }

        [HttpPost]      // POST api/user-authorization/assign-user-voucher
        [Route("assign-user-voucher")]
        public async Task<ActionResult> AssignVoucher([FromBody] UserVoucherModel userVoucher)
        {
            Logger.LogDebug($"Controller action called: UserAuthorizationController.AssignVoucher(...).");
            ProblemDetails problem;
            Logger.LogDebug($"Calling BotUsers.GetByIdAsync(userId: {userVoucher.UserId})...");
            BotUserModel botUser = await BotUsers.GetByIdAsync(userId: userVoucher.UserId);
            if (botUser == null)
            {
                problem = new ProblemDetails();
                problem.Status = 404;
                problem.Title = "User doesn't exist";
                problem.Detail = $"The specified Bot User record could not be found in the DB repository, having ID={userVoucher.UserId}.";
                Logger.LogInformation(problem.Detail);
                return NotFound(problem);
            }
            Logger.LogInformation($"Calling BotUsers.GetByIdAsync(userId: '{userVoucher.UserId}') returned User having record ID={botUser.Id}.");
            VoucherModel voucher = null;
            if (userVoucher.VoucherId.HasValue)
            {
                voucher = await Vouchers.RetrieveAsync(voucherID: userVoucher.VoucherId.Value);
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(userVoucher.Code))
                    voucher = await Vouchers.RetrieveAsync(voucherCode: userVoucher.Code);
            }
            if (voucher == null)
            {
                problem = new ProblemDetails();
                problem.Status = 404;
                problem.Title = "Voucher doesn't exist";
                problem.Detail = $"The specified Bot Voucher record could not be found in the DB repository, having ID={userVoucher.VoucherId} or Code='{userVoucher.Code}'.";
                Logger.LogInformation(problem.Detail);
                return NotFound(problem);
            }
            Logger.LogInformation($"Calling Vouchers.RetrieveAsync(voucherID: {userVoucher.VoucherId} / voucherCode: '{userVoucher.Code}') returned Voucher having record ID={voucher.Id}.");
            if (voucher.AssignedOn.HasValue)
            {
                problem = new ProblemDetails();
                problem.Status = 409;
                problem.Title = "Voucher is assigned";
                problem.Detail = $"The specified Bot Voucher, having record ID={voucher.Id}, seems to have been already assigned on {voucher.AssignedOn}.";
                Logger.LogInformation(problem.Detail);
                return StatusCode(problem.Status.Value, problem);
            }
            if (voucher.ExpiresOn.HasValue && DateTime.Now > voucher.ExpiresOn)
            {
                problem = new ProblemDetails();
                problem.Status = 409;
                problem.Title = "Voucher is expired";
                problem.Detail = $"The specified Bot Voucher, having record ID={voucher.Id}, is expired; it can no longer be used since {voucher.ExpiresOn}.";
                Logger.LogInformation(problem.Detail);
                return StatusCode(problem.Status.Value, problem);
            }
            Logger.LogDebug($"Calling Vouchers.AssignAsync(UserID: {userVoucher.UserId}, VoucherID: {userVoucher.VoucherId})...");
            int? recordId = await Vouchers.AssignAsync(
                voucherID: voucher.Id.Value,
                userID: botUser.Id.Value,
                validFrom: DateTime.Now.AddDays(-1),
                validTo: DateTime.Now.AddDays(voucher.ValidityPeriod)
                );
            if (recordId.HasValue)
            {
                UserVoucherModel theAssignedUserVoucher = await UserVouchers.ValidForUserAsync(userId: botUser.Id.Value);
                Logger.LogInformation($"Voucher ID {theAssignedUserVoucher.Id} ({theAssignedUserVoucher.Code}) was assignd to Bot User ID {botUser.Id}.");
                return Ok(theAssignedUserVoucher);
            }
            else
            {
                problem = new ProblemDetails();
                problem.Status = 500;
                problem.Title = "Could not assign voucher";
                problem.Detail = $"A UserVoucher record could not be persisted in the DB repository for User ID {botUser.Id} and Voucher ID {voucher.Id}.";
                Logger.LogError(problem.Detail);
                return StatusCode(problem.Status.Value, problem);
            }
        }

        /// <summary>
        ///     <para>Retrieves all vouchers that have been historically assigned to a user, including those that may have expired.</para>
        ///     <para>Sorted descending by [Valid_To] date.</para>
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="maxResults"></param>
        /// <returns>Desc-sorted by [Valid_To] list of vouchers, including potentially expired ones.</returns>
        [HttpGet]       // GET api/user-authorization/get-user-vouchers?UserID=7&maxResults=10
        [Route("get-user-vouchers")]
        public async Task<ActionResult> GetUserVouchers(int UserID, int maxResults = 10)
        {
            Logger.LogDebug($"Controller action called: UserAuthorizationController.GetUserVouchers(...).");
            ProblemDetails problem;
            Logger.LogDebug($"Calling UserVouchers.ListForUserAsync(userId: {UserID}, maxRecordsCount: {maxResults})...");
            List<UserVoucherModel> userVoucherList = await UserVouchers.ListForUserAsync(userId: UserID, maxRecordsCount: maxResults);
            if (userVoucherList == null)
            {
                problem = new ProblemDetails();
                problem.Status = 404;
                problem.Title = "No User Vouchers";
                problem.Detail = $"No Vouchers were found assigned to the specified Bot User (with UserID={UserID}).";
                Logger.LogInformation(problem.Detail);
                return NotFound(problem);
            }
            Logger.LogInformation($"Call to UserVouchers.ListForUserAsync(userId: {UserID}, maxRecordsCount: {maxResults}) returned {userVoucherList.Count} results.");
            return Ok(userVoucherList);
        }

        /// <summary>
        ///     <para>Attempts to find a still-valid voucher associated with the user.</para>
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns>A still-valid voucher associated with the user or a 404, if none found.</returns>
        [HttpGet]       // GET api/user-authorization/get-user-voucher?UserID=7
        [Route("get-user-voucher")]
        public async Task<ActionResult> GetUserVoucher(int UserID)
        {
            Logger.LogDebug($"Controller action called: UserAuthorizationController.GetUserVoucher(...).");
            ProblemDetails problem;
            Logger.LogDebug($"Calling UserVouchers.ValidForUserAsync(userId: {UserID})...");
            UserVoucherModel userVoucher = await UserVouchers.ValidForUserAsync(userId: UserID);
            if (userVoucher == null)
            {
                problem = new ProblemDetails();
                problem.Status = 404;
                problem.Title = "Voucher doesn't exist";
                problem.Detail = $"No still-valid User Voucher record was found for User Id='{UserID}'.";
                Logger.LogInformation(problem.Detail);
                return NotFound(problem);
            }
            Logger.LogInformation($"Call to UserVouchers.ValidForUserAsync(userId: {UserID}) returned Voucher ID='{userVoucher.Id}'.");
            return Ok(userVoucher);
        }

        /// <summary>
        ///     <para>Decodes an OAuth JWT token, extracting the claims.</para>
        ///     <para>Note that responses from Google login don't contain an OAuth JWT.</para>
        /// </summary>
        /// <param name="Token"></param>
        /// <returns>A BotUser object, with properties populated from token's claims.</returns>
        [HttpPost]      // POST api/user-authorization/decode-user-token
        [Route("decode-user-token")]
        public ActionResult DecodeUserOAuthJwt([FromBody] string Token)
        {
            Logger.LogDebug($"Controller action called: UserAuthorizationController.DecodeUserOAuthJwt(...).");
            Logger.LogDebug(Token);
            ProblemDetails problem;
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            if (handler.CanReadToken(Token))
            {
                BotUserModel botUserInToken = new BotUserModel();
                JwtSecurityToken decodedToken = handler.ReadJwtToken(Token);
                botUserInToken.Authority = decodedToken.Claims.First(claim => claim.Type == "iss").Value;
                botUserInToken.Email = decodedToken.Claims.First(claim => claim.Type == "email").Value;
                botUserInToken.SubjectId = decodedToken.Claims.First(claim => claim.Type == "sub").Value;
                botUserInToken.SubjectName = decodedToken.Claims.First(claim => claim.Type == "name").Value;
                return Ok(botUserInToken);
            }
            else
            {
                problem = new ProblemDetails();
                problem.Status = 400;
                problem.Title = "Can't read token";
                problem.Detail = $"The passed string could not be parsed into a JWT, a JavaScript Web Token for OAuth.";
                Logger.LogError(problem.Detail);
                return StatusCode(problem.Status.Value, problem);
            }
        }

        /// <summary>
        ///     <para>Decodes an OAuth JWT token, extracting the claims, then retrieves (or creates) a BotUser based on those claims.</para>
        ///     <para>Note that responses from Google login don't contain an OAuth JWT.</para>
        /// </summary>
        /// <param name="Token"></param>
        /// <returns>A BotUser object that is tied to a DB BotUser record; so guaranteed to be persisted.</returns>
        [HttpPost]      // POST api/user-authorization/user-from-token
        [Route("user-from-token")]
        public async Task<ActionResult> GetBotUserFromToken([FromBody] string Token)
        {
            Logger.LogDebug($"Controller action called: UserAuthorizationController.GetBotUserFromToken(...).");
            Logger.LogDebug(Token);
            ProblemDetails problem;
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            if (handler.CanReadToken(Token))
            {
                BotUserModel botUserFromToken = new BotUserModel();
                JwtSecurityToken decodedToken = handler.ReadJwtToken(Token);
                botUserFromToken.Authority = decodedToken.Claims.First(claim => claim.Type == "iss").Value;
                botUserFromToken.Email = decodedToken.Claims.First(claim => claim.Type == "email").Value;
                botUserFromToken.SubjectId = decodedToken.Claims.First(claim => claim.Type == "sub").Value;
                botUserFromToken.SubjectName = decodedToken.Claims.First(claim => claim.Type == "name").Value;
                BotUserModel existingBotUser = await BotUsers.GetByEmailAsync(botUserFromToken.Authority, botUserFromToken.Email, botUserFromToken.SubjectId);
                if (existingBotUser != null)
                {
                    return Ok(botUserFromToken);
                }
                else
                {
                    if (await BotUsers.SaveAsync(botUserFromToken))
                    {
                        return Ok(botUserFromToken);
                    }
                    else
                    {
                        problem = new ProblemDetails();
                        problem.Status = 500;
                        problem.Title = "Could not save user";
                        problem.Detail = $"A Bot User record could not be persisted in the DB repository for Token.Email='{botUserFromToken.Email}' and Token.Issuer='{botUserFromToken.Authority}'.";
                        Logger.LogError(problem.Detail);
                        return StatusCode(problem.Status.Value, problem);
                    }
                }
            }
            else
            {
                problem = new ProblemDetails();
                problem.Status = 400;
                problem.Title = "Can't read token";
                problem.Detail = $"The passed string could not be parsed into a JWT, a JavaScript Web Token for OAuth.";
                Logger.LogError(problem.Detail);
                return StatusCode(problem.Status.Value, problem);
            }
        }
    }
}
