using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using UserNeedsAuthorization.Code;
using System.Threading.Tasks;

namespace UserNeedsAuthorization.Pages
{
    public class WebChatModel : PageModel
    {
        public string BotHandle { get; set; } = null;
        public string EphemeralToken { get; set; } = null;
        public string WebChatUserGUID { get; set; } = null;
        public string WebChatUserName { get; set; } = "Web Chat User";
        private string DirectLineSecret { get; } = null;

        private readonly ILogger<IndexModel> _logger;
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _environment;

        public WebChatModel(ILogger<IndexModel> logger, IConfiguration configuration, IWebHostEnvironment hostingEnvironment)
        {
            this._logger = logger;
            this._configuration = configuration;
            this._environment = hostingEnvironment;
            this.WebChatUserGUID = System.Guid.NewGuid().ToString();
            if (this.User != null && this.User.Identity != null && this.User.Identity.IsAuthenticated)
            {
                if (!string.IsNullOrWhiteSpace(this.User.Identity.Name))
                {
                    this.WebChatUserName = this.User.Identity.Name;
                }
            }
            this.BotHandle = this._configuration["MyBotSettings:HandleName"];
            this.DirectLineSecret = this._configuration["MyBotSettings:DirectLineSecretKey"];
        }
        public async Task<IActionResult> OnGet()
        {
            if (string.IsNullOrWhiteSpace(this.BotHandle)) _logger.LogError("No default bot handle name configured.");
            else _logger.LogDebug($"Default bot handle name configured: {this.BotHandle}");
            if (string.IsNullOrWhiteSpace(this.DirectLineSecret)) _logger.LogError("No default bot WebChat secret configured.");
            else _logger.LogDebug($"Default bot WebChat secret configured: {this.DirectLineSecret}");
            EphemeralToken = await BotUtils.GetEphemeralToken(this.DirectLineSecret);
            return Page();
        }
    }
}
