using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using UserNeedsAuthorization.Code;
using System.Threading.Tasks;

namespace UserNeedsAuthorization.Pages
{
    public class IndexModel : PageModel
    {
        public string BotHandle { get; set; } = null;
        public string EphemeralToken { get; set; } = null;
        private string WebChatSecret { get; } = null;


        private readonly ILogger<IndexModel> _logger;
        private readonly IConfiguration _configuration;

        public IndexModel(ILogger<IndexModel> logger, IConfiguration configuration)
        {
            this._logger = logger;
            this._configuration = configuration;
            this.BotHandle = this._configuration["MyBotSettings:HandleName"];
            this.WebChatSecret = this._configuration["MyBotSettings:WebChatSecretKey"];
        }

        public async Task<IActionResult> OnGet()
        {
            if (string.IsNullOrWhiteSpace(this.BotHandle)) _logger.LogError("No default bot handle name configured.");
            else _logger.LogDebug($"Default bot handle name configured: {this.BotHandle}");
            if (string.IsNullOrWhiteSpace(this.WebChatSecret)) _logger.LogError("No default bot WebChat secret configured.");
            else _logger.LogDebug($"Default bot WebChat secret configured: {this.WebChatSecret}");
            EphemeralToken = await BotUtils.GetEphemeralToken(this.WebChatSecret);
            return Page();
        }
    }
}
