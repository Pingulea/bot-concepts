--=====================================================================================================================
--  DATABASE OBJECTS NEEDED BY THE Reimagine-U Bot and its dependency APIs
-----------------------------------------------------------------------------------------------------------------------
-- These objects are used to keep track of bot users and their authorization to use the bot.
-- Users get permission to engage the bot:
--      - After authentication with one of the supported authentication providers.
--      - Based on vouchers they receive, which have a pre-determined valid period.
--          -   Users that are paying for the service are getting a voucher assigned to them, generated on the fly.
--=====================================================================================================================



IF NOT EXISTS
		(
			SELECT	Schema_Name
			FROM	Information_Schema.Schemata
			WHERE	Schema_Name = 'RU-Bot'
		) 
	BEGIN
		EXEC SP_ExecuteSQL N'CREATE SCHEMA [RU-Bot] AUTHORIZATION [dbo]'
	END
go



--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'RU-Bot' AND Name = N'Users')
CREATE TABLE [RU-Bot].[Users]
	(
	 [User_ID]          Int NOT NULL PRIMARY KEY IDENTITY(0, 1)
	,[Authority]        VarChar(64) NOT NULL
	,[Subject_ID]       VarChar(64) NOT NULL
    ,[Subject_Name]     NVarChar(128) NOT NULL
    ,[Email]            VarChar(128) NOT NULL
    ,[Added_On]         SmallDateTime NOT NULL DEFAULT GETDATE()
    ,[Last_Update]      SmallDateTime NOT NULL DEFAULT GETDATE()
    ,[Last_Activity]    SmallDateTime NOT NULL DEFAULT GETDATE()
    ,[User_GUID]        UniqueIdentifier NOT NULL DEFAULT NEWID()
	)
go

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'RU-Bot' AND Name = N'Vouchers')
CREATE TABLE [RU-Bot].[Vouchers]
	(
     [Voucher_ID]       Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	,[Voucher_Code]     VarChar(64) NOT NULL UNIQUE DEFAULT CAST(NEWID() AS VarChar(128))
    ,[Description]      NVarChar(256) NULL
    ,[Validity_Period]  SmallInt NOT NULL DEFAULT 7
	,[Assigned_On]      SmallDateTime NULL
    ,[Created_On]       SmallDateTime NOT NULL DEFAULT GETDATE()
    ,[Expires_On]       SmallDateTime NULL
	)
go

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'RU-Bot' AND Name = N'User_Vouchers')
CREATE TABLE [RU-Bot].[User_Vouchers]
	(
     [Record_ID]        Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	,[User_ID]          Int NOT NULL FOREIGN KEY REFERENCES [RU-Bot].[Users]([User_ID])
	,[Voucher_Code]     VarChar(64) NOT NULL
    ,[Voucher_ID]       Int NULL FOREIGN KEY REFERENCES [RU-Bot].[Vouchers]([Voucher_ID])
	,[Valid_From]       SmallDateTime NOT NULL DEFAULT GETDATE()
    ,[Valid_To]         SmallDateTime NOT NULL DEFAULT DATEADD(DAY, 7, GETDATE())
	)
go

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


