--=====================================================================================================================
--  STORED PROCEDURES FOR THE Reimagine-U Bot and its dependency APIs
-----------------------------------------------------------------------------------------------------------------------
-- These procedures are used for managing the User records in the DB
--=====================================================================================================================



--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'RU-Bot' AND Name = N'Users___Grid_1')
	DROP PROCEDURE [RU-Bot].[Users___Grid_1]
go
CREATE PROCEDURE [RU-Bot].[Users___Grid_1]
    (
    @MaxRecordsCount Int = 1000
    )
AS
	SELECT TOP (@MaxRecordsCount)
			 [User_ID]          --  Int NOT NULL PRIMARY KEY IDENTITY(0, 1)
	        ,[Authority]        --  VarChar(64) NOT NULL
	        ,[Subject_ID]       --  VarChar(64) NOT NULL
            ,[Subject_Name]     --  NVarChar(128) NOT NULL
            ,[Email]            --  VarChar(128) NOT NULL
            ,[Added_On]         --  SmallDateTime NOT NULL DEFAULT GETDATE()
            ,[Last_Update]      --  SmallDateTime NOT NULL DEFAULT GETDATE()
            ,[Last_Activity]    --  SmallDateTime NOT NULL DEFAULT GETDATE()
            ,[User_GUID]        --  UniqueIdentifier NOT NULL DEFAULT NEWID()
	FROM [RU-Bot].[Users]
	ORDER BY [Added_On] DESC
;
go

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'RU-Bot' AND Name = N'Users___Grid_2')
	DROP PROCEDURE [RU-Bot].[Users___Grid_2]
go
CREATE PROCEDURE [RU-Bot].[Users___Grid_2]
    (
    @MaxRecordsCount Int = 1000
    )
AS
	SELECT TOP (@MaxRecordsCount)
			 [User_ID]          --  Int NOT NULL PRIMARY KEY IDENTITY(0, 1)
	        ,[Authority]        --  VarChar(64) NOT NULL
	        ,[Subject_ID]       --  VarChar(64) NOT NULL
            ,[Subject_Name]     --  NVarChar(128) NOT NULL
            ,[Email]            --  VarChar(128) NOT NULL
            ,[Added_On]         --  SmallDateTime NOT NULL DEFAULT GETDATE()
            ,[Last_Update]      --  SmallDateTime NOT NULL DEFAULT GETDATE()
            ,[Last_Activity]    --  SmallDateTime NOT NULL DEFAULT GETDATE()
            ,[User_GUID]        --  UniqueIdentifier NOT NULL DEFAULT NEWID()
	FROM [RU-Bot].[Users]
	ORDER BY [Last_Update] DESC
;
go

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'RU-Bot' AND Name = N'Users___Grid_3')
	DROP PROCEDURE [RU-Bot].[Users___Grid_3]
go
CREATE PROCEDURE [RU-Bot].[Users___Grid_3]
    (
    @MaxRecordsCount Int = 1000
    )
AS
	SELECT TOP (@MaxRecordsCount)
			 [User_ID]          --  Int NOT NULL PRIMARY KEY IDENTITY(0, 1)
	        ,[Authority]        --  VarChar(64) NOT NULL
	        ,[Subject_ID]       --  VarChar(64) NOT NULL
            ,[Subject_Name]     --  NVarChar(128) NOT NULL
            ,[Email]            --  VarChar(128) NOT NULL
            ,[Added_On]         --  SmallDateTime NOT NULL DEFAULT GETDATE()
            ,[Last_Update]      --  SmallDateTime NOT NULL DEFAULT GETDATE()
            ,[Last_Activity]    --  SmallDateTime NOT NULL DEFAULT GETDATE()
            ,[User_GUID]        --  UniqueIdentifier NOT NULL DEFAULT NEWID()
	FROM [RU-Bot].[Users]
	ORDER BY [Last_Activity] DESC
;
go

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'RU-Bot' AND Name = N'Users___Sel_1')
	DROP PROCEDURE [RU-Bot].[Users___Sel_1]
go
CREATE PROCEDURE [RU-Bot].[Users___Sel_1]
    (
     @Authority         VarChar(64)     -- NOT NULL
    ,@Email             VarChar(128)    -- NOT NULL
    ,@SubjectID         VarChar(64) = NULL
    )
AS
	SELECT TOP (1)
			 [User_ID]          --  Int NOT NULL PRIMARY KEY IDENTITY(0, 1)
	        ,[Authority]        --  VarChar(64) NOT NULL
	        ,[Subject_ID]       --  VarChar(64) NOT NULL
            ,[Subject_Name]     --  NVarChar(128) NOT NULL
            ,[Email]            --  VarChar(128) NOT NULL
            ,[Added_On]         --  SmallDateTime NOT NULL DEFAULT GETDATE()
            ,[Last_Update]      --  SmallDateTime NOT NULL DEFAULT GETDATE()
            ,[Last_Activity]    --  SmallDateTime NOT NULL DEFAULT GETDATE()
            ,[User_GUID]        --  UniqueIdentifier NOT NULL DEFAULT NEWID()
	FROM [RU-Bot].[Users]
	WHERE [Authority] = @Authority
        AND ([Subject_ID] = @SubjectID OR [Email] = @Email)
;
go

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'RU-Bot' AND Name = N'Users___Sel_2')
	DROP PROCEDURE [RU-Bot].[Users___Sel_2]
go
CREATE PROCEDURE [RU-Bot].[Users___Sel_2]
    (
    @UserId         Int     -- NOT NULL PRIMARY KEY IDENTITY(0, 1)
    )
AS
	SELECT TOP (1)
			 [User_ID]          --  Int NOT NULL PRIMARY KEY IDENTITY(0, 1)
	        ,[Authority]        --  VarChar(64) NOT NULL
	        ,[Subject_ID]       --  VarChar(64) NOT NULL
            ,[Subject_Name]     --  NVarChar(128) NOT NULL
            ,[Email]            --  VarChar(128) NOT NULL
            ,[Added_On]         --  SmallDateTime NOT NULL DEFAULT GETDATE()
            ,[Last_Update]      --  SmallDateTime NOT NULL DEFAULT GETDATE()
            ,[Last_Activity]    --  SmallDateTime NOT NULL DEFAULT GETDATE()
            ,[User_GUID]        --  UniqueIdentifier NOT NULL DEFAULT NEWID()
	FROM [RU-Bot].[Users]
	WHERE [User_ID] = @UserId
;
go

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'RU-Bot' AND Name = N'Users___Sel_3')
	DROP PROCEDURE [RU-Bot].[Users___Sel_3]
go
CREATE PROCEDURE [RU-Bot].[Users___Sel_3]
    (
    @UserGUID          UniqueIdentifier     --      DEFAULT NEWID()
    )
AS
	SELECT TOP (1)
			 [User_ID]          --  Int NOT NULL PRIMARY KEY IDENTITY(0, 1)
	        ,[Authority]        --  VarChar(64) NOT NULL
	        ,[Subject_ID]       --  VarChar(64) NOT NULL
            ,[Subject_Name]     --  NVarChar(128) NOT NULL
            ,[Email]            --  VarChar(128) NOT NULL
            ,[Added_On]         --  SmallDateTime NOT NULL DEFAULT GETDATE()
            ,[Last_Update]      --  SmallDateTime NOT NULL DEFAULT GETDATE()
            ,[Last_Activity]    --  SmallDateTime NOT NULL DEFAULT GETDATE()
            ,[User_GUID]        --  UniqueIdentifier NOT NULL DEFAULT NEWID()
	FROM [RU-Bot].[Users]
	WHERE [User_GUID] = @UserGUID
;
go

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'RU-Bot' AND Name = N'Users___Ins')
	DROP PROCEDURE [RU-Bot].[Users___Ins]
go
CREATE PROCEDURE [RU-Bot].[Users___Ins]
	(
	 @UserID            Int OUTPUT
	,@Authority         VarChar(64)                 --      NOT NULL
	,@SubjectID         VarChar(64)                 --      NOT NULL
    ,@SubjectName       NVarChar(128)               --      NOT NULL
    ,@Email             VarChar(128)                --      NOT NULL
    ,@AddedOn           SmallDateTime               --      NOT NULL DEFAULT GETDATE()
    ,@LastUpdate        SmallDateTime               --      NOT NULL DEFAULT GETDATE()
    ,@LastActivity      SmallDateTime               --      NOT NULL DEFAULT GETDATE()
    ,@UserGUID          UniqueIdentifier            --      NOT NULL DEFAULT NEWID()
	)
AS
    SET @AddedOn = ISNULL(@AddedOn, GETDATE());
    SET @LastUpdate = ISNULL(@LastUpdate, GETDATE());
    SET @LastActivity = ISNULL(@LastActivity, GETDATE());
    SET @UserGUID = ISNULL(@UserGUID, NEWID());
	INSERT INTO [RU-Bot].[Users]
        (
         [Authority]        --  VarChar(64) NOT NULL
	    ,[Subject_ID]       --  VarChar(64) NOT NULL
        ,[Subject_Name]     --  NVarChar(128) NOT NULL
        ,[Email]            --  VarChar(128) NOT NULL
        ,[Added_On]         --  SmallDateTime NOT NULL DEFAULT GETDATE()
        ,[Last_Update]      --  SmallDateTime NOT NULL DEFAULT GETDATE()
        ,[Last_Activity]    --  SmallDateTime NOT NULL DEFAULT GETDATE()
        ,[User_GUID]        --  UniqueIdentifier NOT NULL DEFAULT NEWID()
        )
    VALUES
        (
         @Authority         --  VarChar(64) NOT NULL
	    ,@SubjectID         --  VarChar(64) NOT NULL
        ,@SubjectName       --  NVarChar(128) NOT NULL
        ,@Email             --  VarChar(128) NOT NULL
        ,@AddedOn           --  SmallDateTime NOT NULL      --      DEFAULT GETDATE()
        ,@LastUpdate        --  SmallDateTime NOT NULL      --      DEFAULT GETDATE()
        ,@LastActivity      --  SmallDateTime NOT NULL      --      DEFAULT GETDATE()
        ,@UserGUID          --  UniqueIdentifier NOT NULL   --      DEFAULT NEWID()
        )
    SET @UserID = SCOPE_IDENTITY()
;
go

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'RU-Bot' AND Name = N'Users___Upd')
	DROP PROCEDURE [RU-Bot].[Users___Upd]
go
CREATE PROCEDURE [RU-Bot].[Users___Upd]
	(
	 @UserID            Int                     --  NOT NULL PRIMARY KEY IDENTITY(0, 1)
	,@Authority         VarChar(64)             --  NOT NULL
	,@SubjectID         VarChar(64)             --  NOT NULL
    ,@SubjectName       NVarChar(128)           --  NOT NULL
    ,@Email             VarChar(128)            --  NOT NULL
    ,@LastUpdate        SmallDateTime = NULL    --  DEFAULT GETDATE()
	)
AS
    SET @LastUpdate = ISNULL(@LastUpdate, GETDATE());
	UPDATE [RU-Bot].[Users]
	SET
		 [Authority]        =   @Authority          --  VarChar(64) NOT NULL
	    ,[Subject_ID]       =   @SubjectID          --  VarChar(64) NOT NULL
        ,[Subject_Name]     =   @SubjectName        --  NVarChar(128) NOT NULL
        ,[Email]            =   @Email              --  VarChar(128) NOT NULL
        ,[Last_Update]      =   @LastUpdate         --  SmallDateTime NOT NULL DEFAULT GETDATE()
	WHERE [User_ID] = @UserID
;
go

--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'RU-Bot' AND Name = N'Users___Del')
	DROP PROCEDURE [RU-Bot].[Users___Del]
go
CREATE PROCEDURE [RU-Bot].[Users___Del]
	(
	@UserID            Int          --      NOT NULL PRIMARY KEY IDENTITY(0, 1)
	)
AS
	DELETE FROM [RU-Bot].[User_Vouchers] WHERE [User_ID] = @UserID;
	DELETE FROM [RU-Bot].[Users] WHERE [User_ID] = @UserID;
go