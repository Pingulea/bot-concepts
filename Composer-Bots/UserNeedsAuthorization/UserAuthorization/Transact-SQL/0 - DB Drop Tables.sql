--=====================================================================================================================
--  CLEANUP TABLES IF THEY EXIST
-----------------------------------------------------------------------------------------------------------------------
-- This script is only intended during development phase, where tables may need to be redesigned.
-- WARNING: The Users/Vouchers tables will be completely wiped out, along with data they contain.
--=====================================================================================================================



DROP TABLE IF EXISTS [RU-Bot].[User_Vouchers];
DROP TABLE IF EXISTS [RU-Bot].[Vouchers];
DROP TABLE IF EXISTS [RU-Bot].[Users];
go


