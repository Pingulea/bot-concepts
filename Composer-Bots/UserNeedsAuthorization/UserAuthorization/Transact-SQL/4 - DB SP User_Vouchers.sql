--=====================================================================================================================
--  STORED PROCEDURES FOR THE Reimagine-U Bot and its dependency APIs
-----------------------------------------------------------------------------------------------------------------------
-- These procedures are used for managing the Voucher-to-User mappings in the DB
--=====================================================================================================================



--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'RU-Bot' AND Name = N'User_Vouchers___Grid_1')
	DROP PROCEDURE [RU-Bot].[User_Vouchers___Grid_1]
go
CREATE PROCEDURE [RU-Bot].[User_Vouchers___Grid_1]
    (
      @UserID               Int             --  NOT NULL PRIMARY KEY IDENTITY(0, 1)
     ,@MaxRecordsCount      Int = 1000      --  NOT NULL
    )
AS
    SELECT TOP (@MaxRecordsCount)
			     UV.[Record_ID]             --  Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	            ,UV.[User_ID]               --  Int NOT NULL FOREIGN KEY REFERENCES [RU-Bot].[Users]([User_ID])
	            ,UV.[Voucher_Code]          --  VarChar(64) NOT NULL
                ,UV.[Voucher_ID]            --  Int NULL FOREIGN KEY REFERENCES [RU-Bot].[Vouchers]([Voucher_ID])
	            ,UV.[Valid_From]            --  SmallDateTime NOT NULL DEFAULT GETDATE()
                ,UV.[Valid_To]              --  SmallDateTime NOT NULL DEFAULT DATEADD(DAY, 7, GETDATE())
                ,V.[Description]            --  NVarChar(256) NULL
                ,V.[Validity_Period]        --  SmallInt NOT NULL DEFAULT 7
	            ,V.[Assigned_On]            --  SmallDateTime NULL
	    FROM [RU-Bot].[User_Vouchers] UV
            LEFT JOIN [RU-Bot].[Vouchers] V ON UV.[Voucher_ID] = V.[Voucher_ID]
        WHERE UV.[User_ID] = @UserID
	    ORDER BY [Valid_To] DESC
;
go

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'RU-Bot' AND Name = N'User_Vouchers___Sel_1')
	DROP PROCEDURE [RU-Bot].[User_Vouchers___Sel_1]
go
CREATE PROCEDURE [RU-Bot].[User_Vouchers___Sel_1]
    (
    @UserID               Int           -- NOT NULL PRIMARY KEY IDENTITY(0, 1)
    )
AS
    DECLARE @Now SmallDateTime = GETDATE();
	SELECT TOP (1)
			     UV.[Record_ID]         --  Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	            ,UV.[User_ID]           --  Int NOT NULL FOREIGN KEY REFERENCES [RU-Bot].[Users]([User_ID])
	            ,UV.[Voucher_Code]      --  VarChar(64) NOT NULL
                ,UV.[Voucher_ID]        --  Int NULL FOREIGN KEY REFERENCES [RU-Bot].[Vouchers]([Voucher_ID])
	            ,UV.[Valid_From]        --  SmallDateTime NOT NULL DEFAULT GETDATE()
                ,UV.[Valid_To]          --  SmallDateTime NOT NULL DEFAULT DATEADD(DAY, 7, GETDATE())
                ,V.[Description]        --  NVarChar(256) NULL
                ,V.[Validity_Period]    --  SmallInt NOT NULL DEFAULT 7
	            ,V.[Assigned_On]        --  SmallDateTime NULL
	    FROM [RU-Bot].[User_Vouchers] UV
            LEFT JOIN [RU-Bot].[Vouchers] V ON UV.[Voucher_ID] = V.[Voucher_ID]
        WHERE UV.[User_ID] = @UserID
            AND @Now BETWEEN UV.[Valid_From] AND UV.[Valid_To]
	    ORDER BY [Valid_To] DESC
;
go

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'RU-Bot' AND Name = N'User_Vouchers___Expire')
	DROP PROCEDURE [RU-Bot].[User_Vouchers___Expire]
go
CREATE PROCEDURE [RU-Bot].[User_Vouchers___Expire]
    (
    @RecordID               Int         --  NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
    )
AS
	UPDATE [RU-Bot].[User_Vouchers]
        SET [Valid_To] = DATEADD(DAY, -1, GETDATE())
        WHERE [Record_ID] = @RecordID
;
go