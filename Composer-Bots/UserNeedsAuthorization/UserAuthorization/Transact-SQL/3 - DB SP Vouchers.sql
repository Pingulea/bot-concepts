--=====================================================================================================================
--  STORED PROCEDURES FOR THE Reimagine-U Bot and its dependency APIs
-----------------------------------------------------------------------------------------------------------------------
-- These procedures are used for managing the Voucher records in the DB
--=====================================================================================================================



--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'RU-Bot' AND Name = N'Vouchers___Grid_1')
	DROP PROCEDURE [RU-Bot].[Vouchers___Grid_1]
go
CREATE PROCEDURE [RU-Bot].[Vouchers___Grid_1]
    (
      @ExcludeAssigned      Bit = 1         --  NOT NULL
     ,@MaxRecordsCount      Int = 1000      --  NOT NULL
    )
AS
    IF (@ExcludeAssigned > 0)
        BEGIN
	        SELECT TOP (@MaxRecordsCount)
			         [Voucher_ID]       --  Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	                ,[Voucher_Code]     --  VarChar(64) NOT NULL UNIQUE DEFAULT CAST(NEWID() AS VarChar(128))
                    ,[Description]      --  NVarChar(256) NULL
                    ,[Validity_Period]  --  SmallInt NOT NULL DEFAULT 7
	                ,[Assigned_On]      --  SmallDateTime NULL
                    ,[Created_On]       --  SmallDateTime NOT NULL DEFAULT GETDATE()
                    ,[Expires_On]       --  SmallDateTime NULL
	        FROM [RU-Bot].[Vouchers]
            WHERE [Assigned_On] IS NULL
                AND GETDATE() < ISNULL([Expires_On], DATEADD(YEAR, 10, GETDATE()))
	        ORDER BY [Created_On] DESC
        END
    ELSE
        BEGIN
	        SELECT TOP (@MaxRecordsCount)
			         [Voucher_ID]       --  Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	                ,[Voucher_Code]     --  VarChar(64) NOT NULL UNIQUE DEFAULT CAST(NEWID() AS VarChar(128))
                    ,[Description]      --  NVarChar(256) NULL
                    ,[Validity_Period]  --  SmallInt NOT NULL DEFAULT 7
	                ,[Assigned_On]      --  SmallDateTime NULL
                    ,[Created_On]       --  SmallDateTime NOT NULL DEFAULT GETDATE()
                    ,[Expires_On]       --  SmallDateTime NULL
	        FROM [RU-Bot].[Vouchers]
            WHERE GETDATE() < ISNULL([Expires_On], DATEADD(YEAR, 10, GETDATE()))
	        ORDER BY [Created_On] DESC
        END
;
go

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'RU-Bot' AND Name = N'Vouchers___Grid_2')
	DROP PROCEDURE [RU-Bot].[Vouchers___Grid_2]
go
CREATE PROCEDURE [RU-Bot].[Vouchers___Grid_2]
    (
     @DescriptionLike       NVarChar(256) = NULL
    ,@MaxRecordsCount       Int = 1000              --  NOT NULL
    )
AS
    IF (@DescriptionLike IS NULL)
        BEGIN
	        SELECT TOP (@MaxRecordsCount)
			         [Voucher_ID]       --  Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	                ,[Voucher_Code]     --  VarChar(64) NOT NULL UNIQUE DEFAULT CAST(NEWID() AS VarChar(128))
                    ,[Description]      --  NVarChar(256) NULL
                    ,[Validity_Period]  --  SmallInt NOT NULL DEFAULT 7
	                ,[Assigned_On]      --  SmallDateTime NULL
                    ,[Created_On]       --  SmallDateTime NOT NULL DEFAULT GETDATE()
                    ,[Expires_On]       --  SmallDateTime NULL
	        FROM [RU-Bot].[Vouchers]
            WHERE [Assigned_On] IS NULL
                AND GETDATE() < ISNULL([Expires_On], DATEADD(YEAR, 10, GETDATE()))
	        ORDER BY [Created_On] DESC
        END
    ELSE
        BEGIN
	        SELECT TOP (@MaxRecordsCount)
			         [Voucher_ID]       --  Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	                ,[Voucher_Code]     --  VarChar(64) NOT NULL UNIQUE DEFAULT CAST(NEWID() AS VarChar(128))
                    ,[Description]      --  NVarChar(256) NULL
                    ,[Validity_Period]  --  SmallInt NOT NULL DEFAULT 7
	                ,[Assigned_On]      --  SmallDateTime NULL
                    ,[Created_On]       --  SmallDateTime NOT NULL DEFAULT GETDATE()
                    ,[Expires_On]       --  SmallDateTime NULL
	        FROM [RU-Bot].[Vouchers]
            WHERE [Description] LIKE @DescriptionLike
                AND GETDATE() < ISNULL([Expires_On], DATEADD(YEAR, 10, GETDATE()))
	        ORDER BY [Created_On] DESC
        END
;
go

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'RU-Bot' AND Name = N'Vouchers___Sel_1')
	DROP PROCEDURE [RU-Bot].[Vouchers___Sel_1]
go
CREATE PROCEDURE [RU-Bot].[Vouchers___Sel_1]
    (
    @VoucherID      Int         -- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
    )
AS
	SELECT TOP (1)
			 [Voucher_ID]       --  Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	        ,[Voucher_Code]     --  VarChar(64) NOT NULL UNIQUE DEFAULT CAST(NEWID() AS VarChar(128))
            ,[Description]      --  NVarChar(256) NULL
            ,[Validity_Period]  --  SmallInt NOT NULL DEFAULT 7
	        ,[Assigned_On]      --  SmallDateTime NULL
            ,[Created_On]       --  SmallDateTime NOT NULL DEFAULT GETDATE()
            ,[Expires_On]       --  SmallDateTime NULL
	FROM [RU-Bot].[Vouchers]
	WHERE [Voucher_ID] = @VoucherID
;
go

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'RU-Bot' AND Name = N'Vouchers___Sel_2')
	DROP PROCEDURE [RU-Bot].[Vouchers___Sel_2]
go
CREATE PROCEDURE [RU-Bot].[Vouchers___Sel_2]
    (
    @VoucherCode    VarChar(64)     -- NOT NULL DEFAULT CAST(NEWID() AS VarChar(128))
    )
AS
	SELECT TOP (1)
			 [Voucher_ID]           --  Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	        ,[Voucher_Code]         --  VarChar(64) NOT NULL UNIQUE DEFAULT CAST(NEWID() AS VarChar(128))
            ,[Description]          --  NVarChar(256) NULL
            ,[Validity_Period]      --  SmallInt NOT NULL DEFAULT 7
	        ,[Assigned_On]          --  SmallDateTime NULL
            ,[Created_On]           --  SmallDateTime NOT NULL DEFAULT GETDATE()
            ,[Expires_On]           --  SmallDateTime NULL
	FROM [RU-Bot].[Vouchers]
	WHERE [Voucher_Code] = @VoucherCode
;
go

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'RU-Bot' AND Name = N'Vouchers___Ins')
	DROP PROCEDURE [RU-Bot].[Vouchers___Ins]
go
CREATE PROCEDURE [RU-Bot].[Vouchers___Ins]
	(
	 @VoucherID         Int OUTPUT              --  NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	,@VoucherCode       VarChar(64) = NULL      --  UNIQUE DEFAULT CAST(NEWID() AS VarChar(128))
    ,@Description       NVarChar(256) = NULL
    ,@ValidityPeriod    SmallInt = 7            --  NOT NULL DEFAULT 7
	,@AssignedOn        SmallDateTime = NULL
    ,@CreatedOn         SmallDateTime = NULL    --  NOT NULL DEFAULT GETDATE()
    ,@ExpiresOn         SmallDateTime = NULL
	)
AS

    SET @VoucherCode = ISNULL(@VoucherCode, CAST(NEWID() AS VarChar(128)));
    SET @CreatedOn = ISNULL(@CreatedOn, GETDATE());
    DECLARE @ExistingVoucherID Int;
    SET @ExistingVoucherID = (SELECT TOP(1) [Voucher_ID] FROM [RU-Bot].[Vouchers] WHERE [Voucher_Code] = @VoucherCode);
    IF (@ExistingVoucherID IS NULL)
        BEGIN
            INSERT INTO [RU-Bot].[Vouchers]
                (
                 [Voucher_Code]     --  VarChar(64) NOT NULL UNIQUE DEFAULT CAST(NEWID() AS VarChar(128))
                ,[Description]      --  NVarChar(256) NULL
                ,[Validity_Period]  --  SmallInt NOT NULL DEFAULT 7
	            ,[Assigned_On]      --  SmallDateTime NULL
                ,[Created_On]       --  SmallDateTime NOT NULL DEFAULT GETDATE()
                ,[Expires_On]       --  SmallDateTime NULL
                )
            VALUES
                (
                 @VoucherCode       --  VarChar(64) = NULL      --  UNIQUE DEFAULT CAST(NEWID() AS VarChar(128))
                ,@Description       --  NVarChar(256) = NULL
                ,@ValidityPeriod    --  SmallInt = 7            --  NOT NULL DEFAULT 7
	            ,@AssignedOn        --  SmallDateTime = NULL
                ,@CreatedOn         --  SmallDateTime = NULL    --  NOT NULL DEFAULT GETDATE()
                ,@ExpiresOn         --  SmallDateTime = NULL
                );
            SET @VoucherID = SCOPE_IDENTITY();
        END
    ELSE
        BEGIN
            UPDATE [RU-Bot].[Vouchers]
                SET
                     [Description]      = ISNULL(@Description, [Description])   --  NVarChar(256) NULL
                    ,[Validity_Period]  = @ValidityPeriod                       --  SmallInt NOT NULL DEFAULT 7
                    ,[Expires_On]       = @ExpiresOn                            --  SmallDateTime NULL
                WHERE [Voucher_ID] = @ExistingVoucherID;
            SET @VoucherID = @ExistingVoucherID;
        END
;
go

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'RU-Bot' AND Name = N'Vouchers___Assign')
	DROP PROCEDURE [RU-Bot].[Vouchers___Assign]
go
CREATE PROCEDURE [RU-Bot].[Vouchers___Assign]
	(
     @RecordID          Int OUTPUT              --  NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
    ,@VoucherID         Int                     --  NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	,@UserID            Int                     --  NOT NULL PRIMARY KEY IDENTITY(0, 1)
	,@ValidFrom         SmallDateTime = NULL    --  NOT NULL DEFAULT GETDATE()
    ,@ValidTo           SmallDateTime = NULL    --  NOT NULL DEFAULT DATEADD(DAY, 7, GETDATE())
	)
AS
    DECLARE @ValidityPeriod         SmallInt        =   NULL;
    DECLARE @VoucherCode            VarChar(64)     =   NULL;
    DECLARE @VoucherExpirationDate  SmallDateTime   =   NULL;
    SELECT TOP (1)
             @ValidityPeriod            =   [Validity_Period]
            ,@VoucherCode               =   [Voucher_Code]
            ,@VoucherExpirationDate     =   [Expires_On]
        FROM [RU-Bot].[Vouchers]
        WHERE [Voucher_ID] = @VoucherID;
    SET @ValidFrom = ISNULL(@ValidFrom, GETDATE());
    IF (@ValidTo IS NULL)
        BEGIN
            SET @ValidityPeriod = ISNULL(@ValidityPeriod, 7);
            SET @ValidTo = ISNULL(@ValidFrom, DATEADD(DAY, @ValidityPeriod, GETDATE()));
        END;
    SET @VoucherExpirationDate = ISNULL(@VoucherExpirationDate, DATEADD(YEAR, 10, GETDATE()));
    IF (GETDATE() > @VoucherExpirationDate OR @VoucherCode IS NULL)
        BEGIN
            SET @RecordID = NULL;
            RETURN;
        END;
    UPDATE [RU-Bot].[Vouchers]
        SET [Assigned_On] = GETDATE()
        WHERE [Voucher_ID] = @VoucherID;
    INSERT INTO [RU-Bot].[User_Vouchers]
	        (
             [User_ID]          --  Int NOT NULL FOREIGN KEY REFERENCES [RU-Bot].[Users]([User_ID])
	        ,[Voucher_Code]          --  VarChar(64) NOT NULL
            ,[Voucher_ID]       --  Int NULL FOREIGN KEY REFERENCES [RU-Bot].[Vouchers]([Voucher_ID])
	        ,[Valid_From]       --  SmallDateTime NOT NULL DEFAULT GETDATE()
            ,[Valid_To]         --  SmallDateTime NOT NULL DEFAULT DATEADD(DAY, 7, GETDATE())
	        )
        VALUES
            (
             @UserID            --  Int NOT NULL PRIMARY KEY IDENTITY(0, 1)
            ,@VoucherCode       --  VarChar(64);
            ,@VoucherID         --  Int NULL FOREIGN KEY REFERENCES [RU-Bot].[Vouchers]([Voucher_ID])
	        ,@ValidFrom         --  SmallDateTime NOT NULL DEFAULT GETDATE()
            ,@ValidTo           --  SmallDateTime NOT NULL DEFAULT DATEADD(DAY, 7, GETDATE())
            )
    SET @RecordID = SCOPE_IDENTITY()
;
go