--=====================================================================================================================
--  CREATE THE DATABASE USER
-----------------------------------------------------------------------------------------------------------------------
-- Commands to create an user or application account (useful in SQL Azure DBs)
-- This script is intended for use only 1 time per DB instance (dev/test/uat/prod)
--=====================================================================================================================



--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--      Create a SQL Server login
-----------------------------------------------------------------------------------------------------------------------

USE [Master];   --  This will fail with Azure DB; can't change context with USE. Should switch DB from the UI/drop-down
go


CREATE LOGIN [Application.RU-Bot] WITH PASSWORD = N'#Rand0myui345!'
go

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--      Map the server login to a database user account
-----------------------------------------------------------------------------------------------------------------------

USE [Database-Name];   --  This will fail with Azure DB; can't change context with USE. Should switch DB from the UI/drop-down
go

CREATE USER [Application.RU-Bot] FROM LOGIN [Application.RU-Bot] WITH DEFAULT_SCHEMA = [dbo]
go

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--      Grant access to the database user account for various objects or add to a schema
-----------------------------------------------------------------------------------------------------------------------

USE [Database-Name];
go

--  Grant full access to the application account - not recommended!

EXEC SP_AddRoleMember @RoleName = 'Db_Owner', @MemberName = 'Application.RU-Bot'
go

--  Grant to the application account execution rights on only the needed stored procedures

GRANT EXECUTE ON [RU-Bot].[StoredProcedure] TO [Application.RU-Bot]
go

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
