﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RU_BOT.UserAuthorization.Repository
{
    public struct DbInfo
    {
        public static string? ConnectionString;
        public static readonly string ConnectionStringMissingErrorMessage =
            "UserAuthorization.Repository.DbInfo.ConnectionString does not seem initialized. Can't connect to the database!";
    }
}
