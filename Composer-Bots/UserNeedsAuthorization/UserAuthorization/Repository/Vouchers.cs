﻿using RU_BOT.UserAuthorization.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace RU_BOT.UserAuthorization.Repository
{
    public static class Vouchers
    {
        public static async Task<List<VoucherModel>?> GetRecentAsync(int maxRecordsCount = 10, bool excludeAssigned = true)
        {
            if (string.IsNullOrEmpty(DbInfo.ConnectionString) || DbInfo.ConnectionString.Length < 50)
                throw new DataException(DbInfo.ConnectionStringMissingErrorMessage);
            List<VoucherModel>? listOfVouchers = null;
            VoucherModel voucher;
            SqlDataReader sqlDataReader, row;
            SqlCommand sqlCommand = new SqlCommand("[RU-Bot].[Vouchers___Grid_1]");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@ExcludeAssigned", SqlDbType.Bit).Value = excludeAssigned;
            sqlCommand.Parameters.Add("@MaxRecordsCount", SqlDbType.Int).Value = maxRecordsCount;
            using (SqlConnection sqlConnection = new SqlConnection(DbInfo.ConnectionString))
            {
                sqlCommand.Connection = sqlConnection;
                await sqlConnection.OpenAsync();
                row = sqlDataReader = await sqlCommand.ExecuteReaderAsync();
                while (await sqlDataReader.ReadAsync())
                {
                    listOfVouchers ??= new List<VoucherModel>();
                    voucher = new VoucherModel();
                    voucher.Id = (int)row["Voucher_ID"];                                                            //  Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                    voucher.Code = (string)row["Voucher_Code"];                                                     //  VarChar(64) NOT NULL UNIQUE DEFAULT CAST(NEWID() AS VarChar(128))
                    if (row["Description"] != DBNull.Value) voucher.Decription = (string)row["Description"];        //  NVarChar(256) NULL
                    voucher.ValidityPeriod = (short)row["Validity_Period"];                                         //  SmallInt NOT NULL DEFAULT 7
                    if (row["Assigned_On"] != DBNull.Value) voucher.AssignedOn = (DateTime)row["Assigned_On"];      //  SmallDateTime NULL
                    voucher.CreatedOn = (DateTime)row["Created_On"];                                                //  SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Expires_On"] != DBNull.Value) voucher.ExpiresOn = (DateTime)row["Expires_On"];         //  SmallDateTime NULL
                    listOfVouchers.Add(voucher);
                }
                await sqlDataReader.CloseAsync();
                await sqlDataReader.DisposeAsync();
                await sqlCommand.DisposeAsync();
                await sqlConnection.CloseAsync();
            }
            return listOfVouchers;
        }
        public static async Task<List<VoucherModel>?> GetAvailableByDescriptionAsync(string? descriptionLike = null, int maxRecordsCount = 10)
        {
            if (string.IsNullOrEmpty(DbInfo.ConnectionString) || DbInfo.ConnectionString.Length < 50)
                throw new DataException(DbInfo.ConnectionStringMissingErrorMessage);
            List<VoucherModel>? listOfVouchers = null;
            VoucherModel voucher;
            SqlDataReader sqlDataReader, row;
            SqlCommand sqlCommand = new SqlCommand("[RU-Bot].[Vouchers___Grid_2]");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@DescriptionLike", SqlDbType.NVarChar, 256).Value = descriptionLike;
            sqlCommand.Parameters.Add("@MaxRecordsCount", SqlDbType.Int).Value = maxRecordsCount;
            using (SqlConnection sqlConnection = new SqlConnection(DbInfo.ConnectionString))
            {
                sqlCommand.Connection = sqlConnection;
                await sqlConnection.OpenAsync();
                row = sqlDataReader = await sqlCommand.ExecuteReaderAsync();
                while (await sqlDataReader.ReadAsync())
                {
                    listOfVouchers ??= new List<VoucherModel>();
                    voucher = new VoucherModel();
                    voucher.Id = (int)row["Voucher_ID"];                                                            //  Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                    voucher.Code = (string)row["Voucher_Code"];                                                     //  VarChar(64) NOT NULL UNIQUE DEFAULT CAST(NEWID() AS VarChar(128))
                    if (row["Description"] != DBNull.Value) voucher.Decription = (string)row["Description"];        //  NVarChar(256) NULL
                    voucher.ValidityPeriod = (short)row["Validity_Period"];                                         //  SmallInt NOT NULL DEFAULT 7
                    if (row["Assigned_On"] != DBNull.Value) voucher.AssignedOn = (DateTime)row["Assigned_On"];      //  SmallDateTime NULL
                    voucher.CreatedOn = (DateTime)row["Created_On"];                                                //  SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Expires_On"] != DBNull.Value) voucher.ExpiresOn = (DateTime)row["Expires_On"];         //  SmallDateTime NULL
                    listOfVouchers.Add(voucher);
                }
                await sqlDataReader.CloseAsync();
                await sqlDataReader.DisposeAsync();
                await sqlCommand.DisposeAsync();
                await sqlConnection.CloseAsync();
            }
            return listOfVouchers;
        }
        public static async Task<VoucherModel?> RetrieveAsync(int voucherID)
        {
            if (string.IsNullOrEmpty(DbInfo.ConnectionString) || DbInfo.ConnectionString.Length < 50)
                throw new DataException(DbInfo.ConnectionStringMissingErrorMessage);
            VoucherModel? voucher = null;
            SqlDataReader sqlDataReader, row;
            SqlCommand sqlCommand = new SqlCommand("[RU-Bot].[Vouchers___Sel_1]");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@VoucherID", SqlDbType.Int).Value = voucherID;
            using (SqlConnection sqlConnection = new SqlConnection(DbInfo.ConnectionString))
            {
                sqlCommand.Connection = sqlConnection;
                await sqlConnection.OpenAsync();
                row = sqlDataReader = await sqlCommand.ExecuteReaderAsync();
                if (await sqlDataReader.ReadAsync())
                {
                    voucher = new VoucherModel();
                    voucher.Id = (int)row["Voucher_ID"];                                                            //  Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                    voucher.Code = (string)row["Voucher_Code"];                                                     //  VarChar(64) NOT NULL UNIQUE DEFAULT CAST(NEWID() AS VarChar(128))
                    if (row["Description"] != DBNull.Value) voucher.Decription = (string)row["Description"];        //  NVarChar(256) NULL
                    voucher.ValidityPeriod = (short)row["Validity_Period"];                                         //  SmallInt NOT NULL DEFAULT 7
                    if (row["Assigned_On"] != DBNull.Value) voucher.AssignedOn = (DateTime)row["Assigned_On"];      //  SmallDateTime NULL
                    voucher.CreatedOn = (DateTime)row["Created_On"];                                                //  SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Expires_On"] != DBNull.Value) voucher.ExpiresOn = (DateTime)row["Expires_On"];         //  SmallDateTime NULL
                }
                await sqlDataReader.CloseAsync();
                await sqlDataReader.DisposeAsync();
                await sqlCommand.DisposeAsync();
                await sqlConnection.CloseAsync();
            }
            return voucher;
        }
        public static async Task<VoucherModel?> RetrieveAsync(string voucherCode)
        {
            if (string.IsNullOrEmpty(DbInfo.ConnectionString) || DbInfo.ConnectionString.Length < 50)
                throw new DataException(DbInfo.ConnectionStringMissingErrorMessage);
            VoucherModel? voucher = null;
            SqlDataReader sqlDataReader, row;
            SqlCommand sqlCommand = new SqlCommand("[RU-Bot].[Vouchers___Sel_2]");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@VoucherCode", SqlDbType.VarChar, 64).Value = voucherCode;
            using (SqlConnection sqlConnection = new SqlConnection(DbInfo.ConnectionString))
            {
                sqlCommand.Connection = sqlConnection;
                await sqlConnection.OpenAsync();
                row = sqlDataReader = await sqlCommand.ExecuteReaderAsync();
                if (await sqlDataReader.ReadAsync())
                {
                    voucher = new VoucherModel();
                    voucher.Id = (int)row["Voucher_ID"];                                                            //  Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                    voucher.Code = (string)row["Voucher_Code"];                                                     //  VarChar(64) NOT NULL UNIQUE DEFAULT CAST(NEWID() AS VarChar(128))
                    if (row["Description"] != DBNull.Value) voucher.Decription = (string)row["Description"];        //  NVarChar(256) NULL
                    voucher.ValidityPeriod = (short)row["Validity_Period"];                                         //  SmallInt NOT NULL DEFAULT 7
                    if (row["Assigned_On"] != DBNull.Value) voucher.AssignedOn = (DateTime)row["Assigned_On"];      //  SmallDateTime NULL
                    voucher.CreatedOn = (DateTime)row["Created_On"];                                                //  SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Expires_On"] != DBNull.Value) voucher.ExpiresOn = (DateTime)row["Expires_On"];         //  SmallDateTime NULL
                }
                await sqlDataReader.CloseAsync();
                await sqlDataReader.DisposeAsync();
                await sqlCommand.DisposeAsync();
                await sqlConnection.CloseAsync();
            }
            return voucher;
        }
        /// <summary>
        ///     <para>Returns the Record ID of the inserted voucher (or the existing voucher, if the Voucher.Code already exists in the DB.</para>
        ///     <para>If a voucher already exists with same code, only the Description, ValidityPeriod, and ExpiresOn are updated in DB.</para>
        ///     <para>Note that if the voucher code already exists in the DB, that voucher may already be assigned to a user.</para>
        /// </summary>
        /// <param name="voucher"></param>
        /// <returns></returns>
        /// <exception cref="DataException"></exception>
        public static async Task<bool> SaveAsync(VoucherModel voucher)
        {
            if (string.IsNullOrEmpty(DbInfo.ConnectionString) || DbInfo.ConnectionString.Length < 50)
                throw new DataException(DbInfo.ConnectionStringMissingErrorMessage);
            bool success = false;
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = "[RU-Bot].[Vouchers___Ins]";
            sqlCommand.CommandType = CommandType.StoredProcedure;
            using (SqlConnection sqlConnection = new SqlConnection(DbInfo.ConnectionString))
            {
                sqlCommand.Connection = sqlConnection;
                await sqlConnection.OpenAsync();
                sqlCommand.Parameters.Add("@VoucherID", SqlDbType.Int).Direction = ParameterDirection.Output;       //  Int OUTPUT NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                sqlCommand.Parameters.Add("@VoucherCode", SqlDbType.VarChar, 64).Value = voucher.Code;              //  VarChar(64) UNIQUE DEFAULT CAST(NEWID() AS VarChar(128))
                sqlCommand.Parameters.Add("@Description", SqlDbType.NVarChar, 256).Value = voucher.Decription;      //  NVarChar(256) = NULL
                sqlCommand.Parameters.Add("@ValidityPeriod", SqlDbType.SmallInt).Value = voucher.ValidityPeriod;    //  SmallInt NOT NULL DEFAULT 7
                sqlCommand.Parameters.Add("@AssignedOn", SqlDbType.SmallDateTime).Value = voucher.AssignedOn;       //  SmallDateTime = NULL
                sqlCommand.Parameters.Add("@CreatedOn", SqlDbType.SmallDateTime).Value = voucher.CreatedOn;         //  SmallDateTime = NULL NOT NULL DEFAULT GETDATE()
                sqlCommand.Parameters.Add("@ExpiresOn", SqlDbType.SmallDateTime).Value = voucher.ExpiresOn;         //  SmallDateTime = NULL
                if ((await sqlCommand.ExecuteNonQueryAsync()) > 0) success = true;
                if (success) voucher.Id = (int)sqlCommand.Parameters["@VoucherID"].Value;
                await sqlCommand.DisposeAsync();
                await sqlConnection.CloseAsync();
            }
            return success;
        }
        /// <summary>
        ///     <para>Returns the key of the record showing that the voucher was assigned to the user.</para>
        ///     <para>If the voucher is no longer used or there is no such voucher with the record ID being voucherID, then no assignment is being made.</para>
        /// </summary>
        /// <param name="voucherID"></param>
        /// <param name="userID"></param>
        /// <param name="validFrom"></param>
        /// <param name="validTo"></param>
        /// <returns></returns>
        /// <exception cref="DataException"></exception>
        public static async Task<int?> AssignAsync(int voucherID, int userID, DateTime? validFrom, DateTime? validTo)
        {
            if (string.IsNullOrEmpty(DbInfo.ConnectionString) || DbInfo.ConnectionString.Length < 50)
                throw new DataException(DbInfo.ConnectionStringMissingErrorMessage);
            bool success = false;
            int? recordId = null;
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = "[RU-Bot].[Vouchers___Assign]";
            sqlCommand.CommandType = CommandType.StoredProcedure;
            using (SqlConnection sqlConnection = new SqlConnection(DbInfo.ConnectionString))
            {
                sqlCommand.Connection = sqlConnection;
                await sqlConnection.OpenAsync();
                sqlCommand.Parameters.Add("@RecordID", SqlDbType.Int).Direction = ParameterDirection.Output;    //  Int OUTPUT NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                sqlCommand.Parameters.Add("@VoucherID", SqlDbType.Int).Value = voucherID;                       //  Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                sqlCommand.Parameters.Add("@UserID", SqlDbType.Int).Value = userID;                             //  Int NOT NULL PRIMARY KEY IDENTITY(0, 1)
                sqlCommand.Parameters.Add("@ValidFrom", SqlDbType.SmallDateTime).Value = validFrom;             //  SmallDateTime NOT NULL DEFAULT GETDATE()
                sqlCommand.Parameters.Add("@ValidTo", SqlDbType.SmallDateTime).Value = validTo;                 //  SmallDateTime NOT NULL DEFAULT DATEADD(DAY, 7, GETDATE())
                if ((await sqlCommand.ExecuteNonQueryAsync()) > 0) success = true;
                if (success) recordId = (int)sqlCommand.Parameters["@RecordID"].Value;
                await sqlCommand.DisposeAsync();
                await sqlConnection.CloseAsync();
            }
            return recordId;
        }
    }
}
