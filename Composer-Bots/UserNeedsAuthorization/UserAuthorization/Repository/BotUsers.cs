﻿using RU_BOT.UserAuthorization.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace RU_BOT.UserAuthorization.Repository
{
    public class BotUsers
    {
        public static async Task<List<BotUserModel>?> GetRecentlyAddedAsync(int MaxRecordsCount = 100)
        {
            if (string.IsNullOrEmpty(DbInfo.ConnectionString) || DbInfo.ConnectionString.Length < 50)
                throw new DataException(DbInfo.ConnectionStringMissingErrorMessage);
            List<BotUserModel>? listOfBotUsers = null;
            BotUserModel? botUser;
            SqlDataReader sqlDataReader, row;
            SqlCommand sqlCommand = new SqlCommand("[RU-Bot].[Users___Grid_1]");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@MaxRecordsCount", SqlDbType.Int).Value = MaxRecordsCount;
            using (SqlConnection sqlConnection = new SqlConnection(DbInfo.ConnectionString))
            {
                sqlCommand.Connection = sqlConnection;
                await sqlConnection.OpenAsync();
                row = sqlDataReader = await sqlCommand.ExecuteReaderAsync();
                while (await sqlDataReader.ReadAsync())
                {
                    listOfBotUsers ??= new List<BotUserModel>();
                    botUser = new BotUserModel();
                    botUser.Id = (int)row["User_ID"];                       //  Int NOT NULL PRIMARY KEY IDENTITY(0, 1)
                    botUser.Authority = (string)row["Authority"];           //  VarChar(64) NOT NULL
                    botUser.SubjectId = (string)row["Subject_ID"];          //  VarChar(64) NOT NULL
                    botUser.SubjectName = (string)row["Subject_Name"];      //  NVarChar(128) NOT NULL
                    botUser.Email = (string)row["Email"];                   //  VarChar(128) NOT NULL
                    botUser.AddedOn = (DateTime)row["Added_On"];            //  SmallDateTime NOT NULL DEFAULT GETDATE()
                    botUser.LastUpdate = (DateTime)row["Last_Update"];      //  SmallDateTime NOT NULL DEFAULT GETDATE()
                    botUser.LastActivity = (DateTime)row["Last_Activity"];  //  SmallDateTime NOT NULL DEFAULT GETDATE()
                    botUser.GUID = (Guid)row["User_GUID"];                  //  UniqueIdentifier NOT NULL DEFAULT NEWID()
                    listOfBotUsers.Add(botUser);
                }
                await sqlDataReader.CloseAsync();
                await sqlDataReader.DisposeAsync();
                await sqlCommand.DisposeAsync();
                await sqlConnection.CloseAsync();
            }
            return listOfBotUsers;
        }
        public static async Task<BotUserModel?> GetByEmailAsync(string Authority, string Email, string? SubjectID = null)
        {
            if (string.IsNullOrEmpty(DbInfo.ConnectionString) || DbInfo.ConnectionString.Length < 50)
                throw new DataException(DbInfo.ConnectionStringMissingErrorMessage);
            BotUserModel? botUser = null;
            SqlDataReader sqlDataReader, row;
            SqlCommand sqlCommand = new SqlCommand("[RU-Bot].[Users___Sel_1]");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@Authority", SqlDbType.VarChar, 64).Value    = Authority;
            sqlCommand.Parameters.Add("@SubjectID", SqlDbType.VarChar, 64).Value    = SubjectID;
            sqlCommand.Parameters.Add("@Email", SqlDbType.VarChar, 128).Value       = Email;
            using (SqlConnection sqlConnection = new SqlConnection(DbInfo.ConnectionString))
            {
                sqlCommand.Connection = sqlConnection;
                await sqlConnection.OpenAsync();
                row = sqlDataReader = await sqlCommand.ExecuteReaderAsync();
                if (await sqlDataReader.ReadAsync())
                {
                    botUser = new BotUserModel();
                    botUser.Id = (int)row["User_ID"];                       //  Int NOT NULL PRIMARY KEY IDENTITY(0, 1)
                    botUser.Authority = (string)row["Authority"];           //  VarChar(64) NOT NULL
                    botUser.SubjectId = (string)row["Subject_ID"];          //  VarChar(64) NOT NULL
                    botUser.SubjectName = (string)row["Subject_Name"];      //  NVarChar(128) NOT NULL
                    botUser.Email = (string)row["Email"];                   //  VarChar(128) NOT NULL
                    botUser.AddedOn = (DateTime)row["Added_On"];            //  SmallDateTime NOT NULL DEFAULT GETDATE()
                    botUser.LastUpdate = (DateTime)row["Last_Update"];      //  SmallDateTime NOT NULL DEFAULT GETDATE()
                    botUser.LastActivity = (DateTime)row["Last_Activity"];  //  SmallDateTime NOT NULL DEFAULT GETDATE()
                    botUser.GUID = (Guid)row["User_GUID"];                  //  UniqueIdentifier NOT NULL DEFAULT NEWID()
                }
                await sqlDataReader.CloseAsync();
                await sqlDataReader.DisposeAsync();
                await sqlCommand.DisposeAsync();
                await sqlConnection.CloseAsync();
            }
            return botUser;
        }
        public static async Task<BotUserModel?> GetByIdAsync(int userId)
        {
            if (string.IsNullOrEmpty(DbInfo.ConnectionString) || DbInfo.ConnectionString.Length < 50)
                throw new DataException(DbInfo.ConnectionStringMissingErrorMessage);
            BotUserModel? botUser = null;
            SqlDataReader sqlDataReader, row;
            SqlCommand sqlCommand = new SqlCommand("[RU-Bot].[Users___Sel_2]");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@UserId", SqlDbType.Int).Value = userId;
            using (SqlConnection sqlConnection = new SqlConnection(DbInfo.ConnectionString))
            {
                sqlCommand.Connection = sqlConnection;
                await sqlConnection.OpenAsync();
                row = sqlDataReader = await sqlCommand.ExecuteReaderAsync();
                if (await sqlDataReader.ReadAsync())
                {
                    botUser = new BotUserModel();
                    botUser.Id = (int)row["User_ID"];                       //  Int NOT NULL PRIMARY KEY IDENTITY(0, 1)
                    botUser.Authority = (string)row["Authority"];           //  VarChar(64) NOT NULL
                    botUser.SubjectId = (string)row["Subject_ID"];          //  VarChar(64) NOT NULL
                    botUser.SubjectName = (string)row["Subject_Name"];      //  NVarChar(128) NOT NULL
                    botUser.Email = (string)row["Email"];                   //  VarChar(128) NOT NULL
                    botUser.AddedOn = (DateTime)row["Added_On"];            //  SmallDateTime NOT NULL DEFAULT GETDATE()
                    botUser.LastUpdate = (DateTime)row["Last_Update"];      //  SmallDateTime NOT NULL DEFAULT GETDATE()
                    botUser.LastActivity = (DateTime)row["Last_Activity"];  //  SmallDateTime NOT NULL DEFAULT GETDATE()
                    botUser.GUID = (Guid)row["User_GUID"];                  //  UniqueIdentifier NOT NULL DEFAULT NEWID()
                }
                await sqlDataReader.CloseAsync();
                await sqlDataReader.DisposeAsync();
                await sqlCommand.DisposeAsync();
                await sqlConnection.CloseAsync();
            }
            return botUser;
        }

        /// <summary>
        ///     <para>Upserts a bot user record in the DB.</para>
        ///     <para>If the input object has an ID, then the record is updated.</para>
        ///     <para>If the input object has a NULL ID, then the record is inserted, then the ID is populated.</para>
        /// </summary>
        /// <param name="botUser"></param>
        /// <returns>A bool indicating the success & the BotUser.ID is always populated.</returns>
        /// <exception cref="DataException"></exception>
        public static async Task<bool> SaveAsync(BotUserModel botUser)
        {
            if (string.IsNullOrEmpty(DbInfo.ConnectionString) || DbInfo.ConnectionString.Length < 50)
                throw new DataException(DbInfo.ConnectionStringMissingErrorMessage);
            bool success = false;
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            if (string.IsNullOrWhiteSpace(botUser.SubjectName)) botUser.SubjectName = botUser.Email;
            using (SqlConnection sqlConnection = new SqlConnection(DbInfo.ConnectionString))
            {
                sqlCommand.Connection = sqlConnection;
                await sqlConnection.OpenAsync();
                if (botUser.Id.HasValue)
                {
                    sqlCommand.CommandText = "[RU-Bot].[Users___Upd]";
                    sqlCommand.Parameters.Add("@UserID", SqlDbType.Int).Value = botUser.Id.Value;                       //  Int NOT NULL PRIMARY KEY IDENTITY(0, 1)
                    sqlCommand.Parameters.Add("@Authority", SqlDbType.VarChar, 64).Value = botUser.Authority;           //  VarChar(64) NOT NULL
                    sqlCommand.Parameters.Add("@SubjectID", SqlDbType.VarChar, 64).Value = botUser.SubjectId;           //  VarChar(64) NOT NULL
                    sqlCommand.Parameters.Add("@SubjectName", SqlDbType.NVarChar, 128).Value = botUser.SubjectName;     //  NVarChar(128) NOT NULL
                    sqlCommand.Parameters.Add("@Email", SqlDbType.VarChar, 128).Value = botUser.Email;                  //  VarChar(128) NOT NULL
                    sqlCommand.Parameters.Add("@LastUpdate", SqlDbType.SmallDateTime).Value = DateTime.Now;             //  SmallDateTime DEFAULT GETDATE()
                    if ((await sqlCommand.ExecuteNonQueryAsync()) > 0) success = true;
                }
                else
                {
                    sqlCommand.CommandText = "[RU-Bot].[Users___Ins]";
                    sqlCommand.Parameters.Add("@UserID", SqlDbType.Int).Direction = ParameterDirection.Output;          //  Int OUTPUT
                    sqlCommand.Parameters.Add("@Authority", SqlDbType.VarChar, 64).Value = botUser.Authority;           //  VarChar(64) NOT NULL
                    sqlCommand.Parameters.Add("@SubjectID", SqlDbType.VarChar, 64).Value = botUser.SubjectId;           //  VarChar(64) NOT NULL
                    sqlCommand.Parameters.Add("@SubjectName", SqlDbType.NVarChar, 128).Value = botUser.SubjectName;     //  NVarChar(128) NOT NULL
                    sqlCommand.Parameters.Add("@Email", SqlDbType.VarChar, 128).Value = botUser.Email;                  //  VarChar(128) NOT NULL
                    sqlCommand.Parameters.Add("@AddedOn", SqlDbType.SmallDateTime).Value = DateTime.Now;                //  SmallDateTime NOT NULL DEFAULT GETDATE()
                    sqlCommand.Parameters.Add("@LastUpdate", SqlDbType.SmallDateTime).Value = DateTime.Now;             //  SmallDateTime NOT NULL DEFAULT GETDATE()
                    sqlCommand.Parameters.Add("@LastActivity", SqlDbType.SmallDateTime).Value = DateTime.Now;           //  SmallDateTime NOT NULL DEFAULT GETDATE()
                    sqlCommand.Parameters.Add("@UserGUID", SqlDbType.UniqueIdentifier).Value = Guid.NewGuid();          //  UniqueIdentifier NOT NULL DEFAULT NEWID()
                    if ((await sqlCommand.ExecuteNonQueryAsync()) > 0) success = true;
                    if (success) botUser.Id = (int)sqlCommand.Parameters["@UserID"].Value;
                }
                await sqlCommand.DisposeAsync();
                await sqlConnection.CloseAsync();
            }
            return success;
        }
    }
}
