﻿using RU_BOT.UserAuthorization.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace RU_BOT.UserAuthorization.Repository
{
    public static class UserVouchers
    {
        /// <summary>
        ///     <para>Retrieves all vouchers that have been historically assigned to a user, including those that may have expired.</para>
        ///     <para>Sorted descending by [Valid_To] date.</para>
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="maxRecordsCount"></param>
        /// <returns>Desc-sorted by [Valid_To] list of vouchers, including potentially expired ones.</returns>
        /// <exception cref="DataException"></exception>
        public static async Task<List<UserVoucherModel>?> ListForUserAsync(int userId, int maxRecordsCount = 10)
        {
            if (string.IsNullOrEmpty(DbInfo.ConnectionString) || DbInfo.ConnectionString.Length < 50)
                throw new DataException(DbInfo.ConnectionStringMissingErrorMessage);
            List<UserVoucherModel>? listOfVouchers = null;
            UserVoucherModel voucher;
            SqlDataReader sqlDataReader, row;
            SqlCommand sqlCommand = new SqlCommand("[RU-Bot].[User_Vouchers___Grid_1]");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@UserID", SqlDbType.Int).Value = userId;
            sqlCommand.Parameters.Add("@MaxRecordsCount", SqlDbType.Int).Value = maxRecordsCount;
            using (SqlConnection sqlConnection = new SqlConnection(DbInfo.ConnectionString))
            {
                sqlCommand.Connection = sqlConnection;
                await sqlConnection.OpenAsync();
                row = sqlDataReader = await sqlCommand.ExecuteReaderAsync();
                while (await sqlDataReader.ReadAsync())
                {
                    listOfVouchers ??= new List<UserVoucherModel>();
                    voucher = new UserVoucherModel();

                    voucher.Id = (int)row["Record_ID"];                                                                     //  Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                    voucher.UserId = (int)row["User_ID"];                                                                   //  Int NOT NULL FOREIGN KEY REFERENCES [RU-Bot].[Users]([User_ID])
                    voucher.Code = (string)row["Voucher_Code"];                                                             //  VarChar(64) NOT NULL
                    if (row["Voucher_ID"] != DBNull.Value) voucher.VoucherId = (int)row["Voucher_ID"];                      //  Int NULL FOREIGN KEY REFERENCES [RU-Bot].[Vouchers]([Voucher_ID])
                    voucher.ValidFrom = (DateTime)row["Valid_From"];                                                        //  SmallDateTime NOT NULL DEFAULT GETDATE()
                    voucher.ValidTo = (DateTime)row["Valid_To"];                                                            //  SmallDateTime NOT NULL DEFAULT DATEADD(DAY, 7, GETDATE())
                    if (row["Description"] != DBNull.Value) voucher.Description = (string)row["Description"];               //  NVarChar(256) NULL
                    if (row["Validity_Period"] != DBNull.Value) voucher.ValidityPeriod = (short)row["Validity_Period"];     //  SmallInt NOT NULL DEFAULT 7
                    if (row["Assigned_On"] != DBNull.Value) voucher.AssignedOn = (DateTime)row["Assigned_On"];              //  SmallDateTime NULL
                    listOfVouchers.Add(voucher);
                }
                await sqlDataReader.CloseAsync();
                await sqlDataReader.DisposeAsync();
                await sqlCommand.DisposeAsync();
                await sqlConnection.CloseAsync();
            }
            return listOfVouchers;
        }
        /// <summary>
        ///     <para>Attempts to find a still-valid user voucher, that would allow the user to engage in a bot conversation</para>
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        /// <exception cref="DataException"></exception>
        public static async Task<UserVoucherModel?> ValidForUserAsync(int userId)
        {
            if (string.IsNullOrEmpty(DbInfo.ConnectionString) || DbInfo.ConnectionString.Length < 50)
                throw new DataException(DbInfo.ConnectionStringMissingErrorMessage);
            UserVoucherModel? voucher = null;
            SqlDataReader sqlDataReader, row;
            SqlCommand sqlCommand = new SqlCommand("[RU-Bot].[User_Vouchers___Sel_1]");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@UserID", SqlDbType.Int).Value = userId;
            using (SqlConnection sqlConnection = new SqlConnection(DbInfo.ConnectionString))
            {
                sqlCommand.Connection = sqlConnection;
                await sqlConnection.OpenAsync();
                row = sqlDataReader = await sqlCommand.ExecuteReaderAsync();
                if (await sqlDataReader.ReadAsync())
                {
                    voucher = new UserVoucherModel();
                    voucher.Id = (int)row["Record_ID"];                                                                     //  Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                    voucher.UserId = (int)row["User_ID"];                                                                   //  Int NOT NULL FOREIGN KEY REFERENCES [RU-Bot].[Users]([User_ID])
                    voucher.Code = (string)row["Voucher_Code"];                                                             //  VarChar(64) NOT NULL
                    if (row["Voucher_ID"] != DBNull.Value) voucher.VoucherId = (int)row["Voucher_ID"];                      //  Int NULL FOREIGN KEY REFERENCES [RU-Bot].[Vouchers]([Voucher_ID])
                    voucher.ValidFrom = (DateTime)row["Valid_From"];                                                        //  SmallDateTime NOT NULL DEFAULT GETDATE()
                    voucher.ValidTo = (DateTime)row["Valid_To"];                                                            //  SmallDateTime NOT NULL DEFAULT DATEADD(DAY, 7, GETDATE())
                    if (row["Description"] != DBNull.Value) voucher.Description = (string)row["Description"];               //  NVarChar(256) NULL
                    if (row["Validity_Period"] != DBNull.Value) voucher.ValidityPeriod = (short)row["Validity_Period"];     //  SmallInt NOT NULL DEFAULT 7
                    if (row["Assigned_On"] != DBNull.Value) voucher.AssignedOn = (DateTime)row["Assigned_On"];              //  SmallDateTime NULL
                }
                await sqlDataReader.CloseAsync();
                await sqlDataReader.DisposeAsync();
                await sqlCommand.DisposeAsync();
                await sqlConnection.CloseAsync();
            }
            return voucher;
        }
    }
}
