﻿using System;

namespace RU_BOT.UserAuthorization.Models
{
    public class BotUserModel
    {
        /// <summary>
        ///     <para>Internal, app specific identifier of the user record in the database</para>
        /// </summary>
        public int? Id { get; set; }                    //  [User_ID]           Int NOT NULL PRIMARY KEY IDENTITY(0, 1)
        
        /// <summary>
        ///     <para>The name of the trusted authority identifying the user, like Google, Microsoft, GitHub, LinkedIn etc</para>
        /// </summary>
        public string? Authority { get; set; }          //  [Authority]         VarChar(64) NOT NULL

        /// <summary>
        ///     <para>Authority-specific identifier of the user account</para>
        /// </summary>
        public string? SubjectId { get; set; }          //  [Subject_ID]        VarChar(64) NOT NULL

        /// <summary>
        ///     <para>The account/user name, as provided by the authentication authority</para>
        /// </summary>
        public string? SubjectName { get; set; }        //  [Subject_Name]      NVarChar(128) NOT NULL

        /// <summary>
        ///     <para>Value of the 'email' claim, as provided by the authentication authority</para>
        /// </summary>
        public string? Email { get; set; }              //  [Email]             VarChar(128) NOT NULL

        /// <summary>
        ///     <para>When was the user account first added to the bot's DB</para>
        /// </summary>
        public DateTime AddedOn { get; set; }           //  [Added_On]          SmallDateTime NOT NULL DEFAULT GETDATE()

        public DateTime LastUpdate { get; set; }        //  [Last_Update]       SmallDateTime NOT NULL DEFAULT GETDATE()

        public DateTime LastActivity { get; set; }      //  [Last_Activity]     SmallDateTime NOT NULL DEFAULT GETDATE()

        public Guid GUID { get; set; }                  //  [User_GUID]         UniqueIdentifier NOT NULL DEFAULT NEWID()
    }
}
