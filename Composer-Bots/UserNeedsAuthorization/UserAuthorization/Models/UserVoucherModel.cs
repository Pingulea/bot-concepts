﻿using System;

namespace RU_BOT.UserAuthorization.Models
{
    /// <summary>
    ///     <para>Models the vouchers that have been assigned to users, ensuring the user are authorized to use the bot</para>
    /// </summary>
    public class UserVoucherModel
    {
        /// <summary>
        ///     <para>Identifies the DB record where the voucher is associated to a user</para>
        /// </summary>
        public int Id { get; set; }                     //  [Record_ID]         Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)

        /// <summary>
        ///     <para>Identifies the BotUser to which the voucher was assigned</para>
        /// </summary>
        public int UserId { get; set; }                 //  [User_ID]           Int NOT NULL FOREIGN KEY REFERENCES[RU - Bot].[Users] ([User_ID])

        /// <summary>
        ///     <para>The code appearing on the voucher, that the user is presenting to redeem a voucher</para>
        /// </summary>
        public string? Code { get; set; }               //  [Voucher_Code]      VarChar(64) NOT NULL

        /// <summary>
        ///     <para>Identifies a previously generated voucher that the user has redeemed</para>
        ///     <para>Vouchers can also be assigned ad-hoc by bot admins; hence, the user may haven't redeemed a voucher, one was assigned on the fly</para>
        /// </summary>
        public int? VoucherId { get; set; }             //  [Voucher_ID]        Int NULL FOREIGN KEY REFERENCES[RU - Bot].[Vouchers] ([Voucher_ID])

        /// <summary>
        ///     <para>Date from which the voucher is valid for the user</para>
        /// </summary>
        public DateTime ValidFrom { get; set; }         //  [Valid_From]        SmallDateTime NOT NULL DEFAULT GETDATE()

        /// <summary>
        ///     <para>Date until this voucher is valid to authorize the user for engaging the bot</para>
        /// </summary>
        public DateTime ValidTo { get; set; }           //  [Valid_To]          SmallDateTime NOT NULL DEFAULT DATEADD(DAY, 7, GETDATE())

        public string? Description { get; set; }        //  [Description]       NVarChar(256) NULL

        public short? ValidityPeriod { get; set; }      //  [Validity_Period]   SmallInt NOT NULL DEFAULT 7 (but LEFT JOIN)

        public DateTime? AssignedOn { get; set; }       //  [Assigned_On]       SmallDateTime NULL
    }
}
