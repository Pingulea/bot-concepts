﻿using System;

namespace RU_BOT.UserAuthorization.Models
{
    /// <summary>
    ///     <para>Models vouchers that can be generated and then assigned to bot users.</para>
    ///     <para>Users may redeem these vouchers; once this is done, it becomes a UserVoucher.</para>
    /// </summary>
    public class VoucherModel
    {
        /// <summary>
        ///     <para>The Record ID in the DB for a voucher that can be assigned</para>
        /// </summary>
        public int? Id { get; set; }                    //  [Voucher_ID]        Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)

        /// <summary>
        ///     <para>The code on the voucher, that the user is showing to get access</para>
        /// </summary>
        public string? Code { get; set; }               //  [Voucher_Code]      VarChar(64) NOT NULL UNIQUE DEFAULT CAST(NEWID() AS VarChar(128))

        /// <summary>
        ///     <para>The type of voucher; could be used as a voucher category field</para>
        /// </summary>
        public string? Decription { get; set; }         //  [Description]       NVarChar(256) NULL

        /// <summary>
        ///     <para>How many days of bot usage does this voucher provide?</para>
        ///     <para>From the moment the voucher is redeemed, the user gets this many days allowance for engaging the bot.</para>
        ///     <para>The default value is 7 (days).</para>
        /// </summary>
        public short ValidityPeriod { get; set; } = 7;  //  [Validity_Period]   SmallInt NOT NULL DEFAULT 7

        /// <summary>
        ///     <para>Tells when the voucher was assigned to a user.</para>
        ///     <para>If null, the voucher is not yet given to a user.</para>
        /// </summary>
        public DateTime? AssignedOn { get; set; }       //  [Assigned_On]       SmallDateTime NULL

        /// <summary>
        ///     <para>Informational field: tells when the voucher was created in the system.</para>
        /// </summary>
        public DateTime? CreatedOn { get; set; }         //  [Created_On]        SmallDateTime NOT NULL DEFAULT GETDATE()

        /// <summary>
        ///     <para>Tells until when a voucher may be used (redeemed).</para>
        ///     <para>After this date, attempts to redeem the voucher should fail.</para>
        /// </summary>
        public DateTime? ExpiresOn { get; set; }        //  [Expires_On]        SmallDateTime NULL
    }
}
