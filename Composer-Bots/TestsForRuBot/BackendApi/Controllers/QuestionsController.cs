﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BackendApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuestionsController : ControllerBase
    {
        public static List<QuestionModel>? Questions;

        public QuestionsController()
        {
            if (Questions == null)
            {
                Questions = new List<QuestionModel>();
                QuestionModel question;
                AnswerOptionModel answerOption;

                question = new QuestionModel()
                {
                    QuestionId = 14,
                    Type = "Free-text",
                    Text = "What's your name?"
                };
                Questions.Add(question);

                question = new QuestionModel()
                {
                    QuestionId = 15,
                    Type = "Continue",
                    Text = "Please press CONTINUE."
                };
                Questions.Add(question);

                question = new QuestionModel()
                {
                    QuestionId = 16,
                    Type = "Multi-option",
                    Text = "Given the question here, what's your answer now?",
                    Options = new List<AnswerOptionModel>()
                };
                answerOption = new AnswerOptionModel() { AnswerID = 101, Text = "I'm picking first" };
                question.Options.Add(answerOption);
                answerOption = new AnswerOptionModel() { AnswerID = 102, Text = "I'll choose second" };
                question.Options.Add(answerOption);
                answerOption = new AnswerOptionModel() { AnswerID = 103, Text = "Let's consider third" };
                question.Options.Add(answerOption);
                Questions.Add(question);
            }
        }

        // GET: api/<QuestionsController>
        [HttpGet]
        public ActionResult Get()
        {
            ProblemDetails problem;
            if (QuestionsController.Questions == null)
            {
                problem = new ProblemDetails();
                problem.Status = 404;
                problem.Title = "Questions list doesn't exist";
                problem.Detail = $"The QuestionsController.Questions object is null.";
                return NotFound(problem);
            }
            if (QuestionsController.Questions.Count < 1)
            {
                problem = new ProblemDetails();
                problem.Status = 404;
                problem.Title = "Questions list is empty";
                problem.Detail = $"The QuestionsController.Questions list object does not contain any QuestionModel instance.";
                return NotFound(problem);
            }
            return Ok(QuestionsController.Questions);
        }

        // GET api/<QuestionsController>/5
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            foreach (QuestionModel question in QuestionsController.Questions)
            {
                if (question.QuestionId == id) return Ok(question);
            }
            ProblemDetails problem;
            problem = new ProblemDetails();
            problem.Status = 404;
            problem.Title = "Question doesn't exist";
            problem.Detail = $"No question was found having QuestionID = {id}.";
            return NotFound(problem);
        }

        // POST api/<QuestionsController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<QuestionsController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<QuestionsController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
