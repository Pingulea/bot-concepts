﻿namespace BackendApi
{
    public class AnswerOptionModel
    {
        public int AnswerID { get; set; } = -1;
        public string Text { get; set; } = "[No Answer]";
    }    

    public class QuestionModel
    {
        public int QuestionId { get; set; }
        public string Type { get; set; } = "Simple-text";

        public string Text { get; set; } = "What's your name, buddy?";

        public List<AnswerOptionModel>? Options { get; set; }
    }
}
